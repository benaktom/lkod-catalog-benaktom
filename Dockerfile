FROM node:18-alpine AS builder
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
COPY .env.template ./.env
RUN npm run build


FROM node:18-alpine
WORKDIR /app

# Create a non-root user
RUN addgroup -g 1001 -S nonroot
RUN adduser -S nonroot -u 1001
USER nonroot

COPY --from=builder /app/public ./public
COPY --from=builder /app/next.config.js ./next.config.js
COPY --from=builder /app/.env.template ./.env
COPY --from=builder --chown=nonroot:nonroot /app/.next ./.next
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package.json ./package.json

EXPOSE 3000
CMD ["npm", "run", "start"]
