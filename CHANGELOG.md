# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.2] - 2023-07-11

### Changed

- Site layout change according to the source SPARQL sending topics and publisher metadata ([lkod-catalog#9](https://gitlab.com/operator-ict/golemio/lkod/lkod-catalog/-/issues/9))

## [1.0.1] - 2023-07-03

### Added

- Organization page URL ([lkod-catalog#94](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/issues/94))

## [1.0.0] - 2023-06-27

### Added

- Add `CHANGELOG.md` file

### Changed

- New oict logo