const additionalPages = ["/datasets/"];

module.exports = {
    siteUrl: process.env.WEBSITE_URL || "https://opendata.praha.eu",
    generateRobotsTxt: true,
    generateIndexSitemap: false,
    exclude: ["/server-sitemap.xml"],
    robotsTxtOptions: {
        additionalSitemaps: ["https://opendata.praha.eu/server-sitemap.xml"],
        policies: [
            {
                userAgent: "*",
                allow: "/",
            },
        ],
    },
    additionalPaths: async (config) => {
        const result = [];
        for (const page of additionalPages) {
            result.push(await config.transform(config, page));
        }
        return result;
    },
};
