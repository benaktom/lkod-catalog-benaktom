declare namespace NodeJS {
    export interface ProcessEnv {
        readonly ENDPOINT: string;
        readonly PUBLIC_URL: STRING;
        readonly ADMIN_URL: string;
    }
}
