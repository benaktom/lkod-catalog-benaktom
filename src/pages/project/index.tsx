import type { NextPage } from "next";
import Link from "next/link";
import React from "react";
import { useTranslation } from "react-i18next";

import { Breadcrumb } from "@/components/Breadcrumb";
import { BackgroundPicture } from "@/components/configurable/BackgroundPicture";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import Meta from "@/root/metatags.json";
import styles from "@/styles/Project.module.scss";

const PAGE_LABEL = Meta.projectPage.title;

const Project: NextPage = () => {
    const { t } = useTranslation();
    return (
        <Container>
            <Breadcrumb labels={[{ label: PAGE_LABEL, link: "" }]} />
            <BackgroundPicture />
            <Heading tag={`h1`}>{PAGE_LABEL}</Heading>
            <div className={styles["project-container"]}>
                <p>{t("projectpage.aboutCatalog")}</p>
                <Heading tag={`h2`}>{t("projectpage.aboutOpenDataTitle")}</Heading>
                <ul>
                    <li className={styles.heading}>{t("projectpage.aboutOpenDataSubTitle")}</li>
                    <li>{t("projectpage.aboutOpenDataparagraph1")}</li>
                    <li>{t("projectpage.aboutOpenDataparagraph2")}</li>
                    <li>{t("projectpage.aboutOpenDataparagraph3")}</li>
                    <li>{t("projectpage.aboutOpenDataparagraph4")}</li>
                    <li className={styles.heading}>{t("projectpage.openDataMustBeSubTitle")}</li>
                    <li>{t("projectpage.openDataMustBeparagraph1")}</li>
                    <li>{t("projectpage.openDataMustBeparagraph2")}</li>
                </ul>
                <p>{t("projectpage.aboutOpenData")}</p>
                <Heading tag={`h2`}>{t("projectpage.aboutOpenDatainPrague")}</Heading>
                <p>
                    {t("projectpage.paragraph1")}{" "}
                    <Link
                        href={`https://www.praha.eu/jnp/cz/o_meste/primator_a_volene_organy/rada/programove_prohlaseni_rady_hlavniho_1.html`}
                    >
                        <a>{t("projectpage.paragraph2")}</a>
                    </Link>{" "}
                    {t("projectpage.paragraph3")}{" "}
                    <Link
                        href={`https://www.praha.eu/jnp/cz/o_meste/primator_a_volene_organy/rada/komise_rady/volebni_obdobi_2014_2018/komise_rady_hmp_pro_ICT_/zapisy_z_jednani/Zapis_z_jednani_komise_4.html`}
                    >
                        <a>{t("projectpage.paragraph4")}</a>
                    </Link>{" "}
                    {t("projectpage.paragraph5")}
                </p>
                <Heading tag={`h2`}>{t("projectpage.lkodTitle")}</Heading>
                <p
                    dangerouslySetInnerHTML={{
                        __html: t("projectpage.lkodSubTitle"),
                    }}
                ></p>
                <Heading tag={`h2`}>{t("projectpage.contactsTitle")}</Heading>
                <p>
                    {t("projectpage.contactsparagraph1")}{" "}
                    <a href={`mailto:${t("projectpage.contactsMail")}`}>{t("projectpage.contactsMail")}</a>
                    {". "}
                    {t("projectpage.contactsparagraph2")}
                    <a href={`mailto:${t("projectpage.contactsMailopendata")}`}>{t("projectpage.contactsMailopendata")}</a>.
                </p>
            </div>
        </Container>
    );
};

export const getServerSideProps = async () => {
    return {
        props: {
            metaData: {
                title: Meta.projectPage.title,
                ogImage: Meta.projectPage.ogImage,
                twitterImage: Meta.projectPage.twitterImage,
            },
        },
    };
};

export default Project;
