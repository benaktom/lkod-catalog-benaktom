import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { useTranslation } from "react-i18next";

import { CardInfo } from "@/components/CardInfo";
import ColorLink from "@/components/ColorLink";
import { Heading } from "@/components/Heading";
import { NoResultIcon } from "@/components/icons/NoResultIcon";
import { Text } from "@/components/Text";

import classes from "./FourOhFour.module.scss";

const FourOhFour = () => {
    const { t } = useTranslation();
    const router = useRouter();

    return (
        <main className={classes.main}>
            <div className={classes.container}>
                <CardInfo className={classes["four-oh-four"]}>
                    <NoResultIcon />
                    <Heading tag={`h1`} type={`h3`}>
                        {t("404.pagedoesnotexist")}
                    </Heading>
                    <Text>
                        {t("404.linkfilters")} <Link href={`/`}>{t("404.linkfilters2")}</Link>.
                    </Text>
                    <ColorLink linkText={t("buttons.back")} onClick={() => router.back()} direction={`left`} center />
                </CardInfo>
            </div>
        </main>
    );
};

export default FourOhFour;
