import type { NextPage } from "next";
import React from "react";
import { Trans, useTranslation } from "react-i18next";

import { Breadcrumb } from "@/components/Breadcrumb";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import Meta from "@/root/metatags.json";
import styles from "@/styles/Accessibility.module.scss";

const PAGE_LABEL = Meta.accessibilityPage.title;

const Project: NextPage = () => {
    const { t } = useTranslation();

    return (
        <Container>
            <Breadcrumb labels={[{ label: PAGE_LABEL, link: "" }]} />
            <Heading tag={`h1`}>{PAGE_LABEL}</Heading>
            <section className={styles["text"]}>
                <p>{t("accessibility.accessibilityDescription")}</p>
                <p>
                    <Trans
                        i18nKey={t("accessibility.accessibilityDescriptionP2")}
                        components={{
                            link1: <a href={`https://opendata.praha.eu`} target="_blank" rel="noreferrer" />,
                        }}
                    />
                </p>
                <Heading tag={`h2`}>{t("accessibility.complianceStatus")}</Heading>
                <p>{t("accessibility.complianceStatusDescription")}</p>
                <Heading tag={`h2`}>{t("accessibility.accessibilityStatementTitle")}</Heading>
                <p>{t("accessibility.accessibilityStatementDescription")}</p>
                <p>{t("accessibility.accessibilityStatementDescriptionP2")}</p>
                <p>{t("accessibility.accessibilityStatementDescriptionP3")}</p>
                <Heading tag={`h2`}>{t("accessibility.feedbackAndContactTitle")}</Heading>
                <p>
                    {t("accessibility.feedbackAndContactDescription")}
                    <a href={`mailto:${t("accessibility.golemioEmail")}`}> {t("accessibility.golemioEmail")}</a>
                </p>
                <Heading tag={`h2`}>{t("accessibility.lawEnforcementTitle")}</Heading>
                <p>{t("accessibility.lawEnforcementDescription")}</p>
                <p>{t("accessibility.lawEnforcementDescriptionP2")} </p>
                <p>{t("accessibility.lawEnforcementDescriptionP3")} </p>
                <p>
                    email: <a href={`mailto:${t("accessibility.email")}`}>{t("accessibility.email")}</a>
                </p>
            </section>
        </Container>
    );
};

export const getServerSideProps = async () => {
    return {
        props: {
            metaData: {
                title: Meta.accessibilityPage.title,
            },
        },
    };
};

export default Project;
