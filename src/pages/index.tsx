import type { GetServerSideProps, NextPage } from "next";
import React, { FC, useRef } from "react";
import { useTranslation } from "react-i18next";

import ColorLink from "@/components/ColorLink";
import { BackgroundPicture } from "@/components/configurable/BackgroundPicture";
import { SiteTitle } from "@/components/configurable/SiteTitle/index";
import { ProjectInfo } from "@/components/ProjectInfo";
import { Counter } from "@/modules/counter/Counter";
import HorizontalList from "@/modules/organizations/HorizontalList";
import { SearchForm } from "@/modules/search/SearchForm";
import TopicList from "@/modules/topics/TopicList";
import { CatalogService } from "@/root/services/catalog-service";
import { IPublisherData, IThemeData } from "@/schema/common";
import styles from "@/styles/Home.module.scss";

const service = new CatalogService();

type Page = NextPage & {
    organizations: IPublisherData[];
    topics: IThemeData[];
    count: number;
};

const Home: FC<Page> = ({ organizations, topics, count }) => {
    const topicsRef = useRef<HTMLDivElement>(null);
    const { t } = useTranslation();
    const scrollToClick = () => {
        topicsRef.current?.scrollIntoView({ behavior: "smooth", block: "start" });
    };

    return (
        <main id="main" className={styles.main}>
            <BackgroundPicture isHomepage />
            <div className={styles.container}>
                <SiteTitle />
                {(organizations.length !== 0 || topics.length !== 0) && (
                    <Counter
                        orgsCount={organizations.length}
                        datasetsCount={count}
                        themesCount={topics.length.toString()}
                        onClick={scrollToClick}
                    />
                )}
                <SearchForm />
                <ColorLink
                    className={styles["color-link"]}
                    linkText={t("colorLink.allDatasets")}
                    linkUrl="/datasets"
                    direction={`right`}
                />
                {topics.length !== 0 && <TopicList label={t("mainPage.topics")} data={topics} ref={topicsRef} id="topicList" />}
            </div>
            {organizations.length !== 0 && <HorizontalList label={t("mainPage.organizations")} data={organizations} />}
            <ProjectInfo />
        </main>
    );
};

export const getServerSideProps: GetServerSideProps<{
    organizations: IPublisherData[];
    topics: IThemeData[];
    count: number;
}> = async ({ res }) => {
    const organizations = await service.getAllPublishersData();
    const topics = await service.getAllThemesData();
    const { count } = await service.findDatasets({
        offset: 0,
    });

    res.setHeader("Cache-Control", "public, s-maxage=10, stale-while-revalidate=59");

    return {
        props: { organizations, topics, count },
    };
};

export default Home;
