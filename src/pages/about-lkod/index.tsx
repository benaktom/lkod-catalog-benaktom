import type { NextPage } from "next";
import Image from "next/image";
import React from "react";
import { Trans, useTranslation } from "react-i18next";

import { Breadcrumb } from "@/components/Breadcrumb";
import { Card } from "@/components/Card";
import { BackgroundPicture } from "@/components/configurable/BackgroundPicture";
import { Heading } from "@/components/Heading";
import AccessibilityIcon from "@/components/icons/AccessibilityIcon";
import ListIcon from "@/components/icons/ListIcon";
import OfnIcon from "@/components/icons/OfnIcon";
import OpenSourceIcon from "@/components/icons/OpenSourceIcon";
import ImageChange from "@/components/ImageChange";
import { Text } from "@/components/Text";
import hierarchImg from "@/images/hierarchy.webp";
import LKOD_Administrace from "@/images/lkod-administration.webp";
import Meta from "@/root/metatags.json";
import styles from "@/styles/about-lkod.module.scss";

const PAGE_LABEL = Meta.aboutPage.title;

const AboutLkod: NextPage = () => {
    const { t } = useTranslation();

    return (
        <main id="main" className={styles.main}>
            <div className={styles.container}>
                <Breadcrumb labels={[{ label: PAGE_LABEL, link: "" }]} />
                <BackgroundPicture />
                <div className={styles["aboutlkod-container"]}>
                    <Heading tag={`h1`}>{PAGE_LABEL}</Heading>
                    <p>{t("aboutLkodPage.aboutLkodPageDescription")}</p>
                    <Heading className={styles["heading"]} tag={`h2`}>
                        {t("aboutLkodPage.lkod")}
                    </Heading>
                    <section className={styles["container-purpose"]}>
                        <div className={styles["text-field"]}>
                            <h3>{t("aboutLkodPage.whatIsLC")}</h3>
                            <Card tag="div" className={styles["text-field-card"]}>
                                <ListIcon values="1" />
                                <h4>{t("aboutLkodPage.whatIsLCparagraph1")}</h4>
                            </Card>
                            <Card tag="div" className={styles["text-field-card"]}>
                                <ListIcon values="2" />
                                <h4>{t("aboutLkodPage.whatIsLCparagraph2")}</h4>
                            </Card>
                        </div>
                        <div className={styles["image-field"]}>
                            <h3>{t("aboutLkodPage.hierarchyLC")}</h3>
                            <Image
                                src={hierarchImg}
                                alt="catalog hierarchy image"
                                width={586}
                                height={360}
                                layout="responsive"
                                objectFit="contain"
                            />
                        </div>
                    </section>

                    <Heading className={styles["heading"]} tag={`h2`}>
                        {t("aboutLkodPage.wantToUseLKODTitle")}
                    </Heading>

                    <section className={styles["container-uselkod"]}>
                        <div className={styles["uselkod-card"]}>
                            <h3>{t("aboutLkodPage.forOrganizationsTitle")}</h3>
                            <p>
                                <Trans
                                    i18nKey="aboutLkodPage.forOrganizations"
                                    t={t}
                                    components={{
                                        link1: (
                                            <a
                                                href={`https://opendata.praha.eu/`}
                                                title="link1"
                                                target="_blank"
                                                rel="noreferrer"
                                            />
                                        ),
                                    }}
                                />
                                <a href={`mailto:${t("aboutLkodPage.emailForOrganizations")}`}>
                                    {t("aboutLkodPage.emailForOrganizations")}
                                </a>
                            </p>
                        </div>
                        <div className={styles["uselkod-card"]}>
                            <h3>{t("aboutLkodPage.lCTitle")}</h3>
                            <p>
                                <Trans
                                    i18nKey="aboutLkodPage.lCDescription"
                                    t={t}
                                    components={{
                                        link1: (
                                            <a
                                                href={`https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/tree/master/demo`}
                                                target="_blank"
                                                rel="noreferrer"
                                            />
                                        ),
                                    }}
                                />
                            </p>
                        </div>
                        <div className={styles["uselkod-card"]}>
                            <h3>{t("aboutLkodPage.migrationTitle")}</h3>
                            <p>
                                <Trans
                                    i18nKey="aboutLkodPage.migrationDescription"
                                    t={t}
                                    components={{
                                        link1: (
                                            <a
                                                href={"https://lkod-migration.rabin.golemio.cz"}
                                                target="_blank"
                                                rel="noreferrer"
                                            />
                                        ),
                                    }}
                                />
                            </p>
                        </div>
                    </section>

                    <Heading className={styles["heading"]} tag={`h2`}>
                        {t("aboutLkodPage.technicallyLKODTitle")}
                    </Heading>

                    <section className={styles["container-lkodtech"]}>
                        <div className={styles["lkodtech-text"]}>
                            <p>{t("aboutLkodPage.technicallyLKODDescription")}</p>
                        </div>
                        <div className={styles["lkodtech-image"]}>
                            <ImageChange />
                        </div>
                    </section>
                </div>
            </div>
            <div className={styles["administration-block-wrapper"]}>
                <div className={styles["administration-block"]}>
                    <Heading className={styles["heading"]} tag={`h2`}>
                        {t("aboutLkodPage.lKODAdministrationTitle")}
                    </Heading>
                    <div className={styles["administration-list"]}>
                        <p>{t("aboutLkodPage.lKODAdministrationDescription")}</p>

                        <ul>
                            <li>
                                <ListIcon color="primary" width="24px" height="24px" values="1" />
                                <p>{t("aboutLkodPage.lKODAdministrationparagraph1")}</p>
                            </li>

                            <li>
                                <ListIcon color="primary" width="24px" height="24px" values="2" />
                                {t("aboutLkodPage.lKODAdministrationparagraph2")}
                            </li>
                            <li>
                                <ListIcon color="primary" width="24px" height="24px" values="3" />
                                {t("aboutLkodPage.lKODAdministrationparagraph3")}
                            </li>
                            <li>
                                <ListIcon color="primary" width="24px" height="24px" values="4" />
                                {t("aboutLkodPage.lKODAdministrationparagraph4")}
                            </li>
                            <li>
                                <ListIcon color="primary" width="24px" height="24px" values="5" />
                                {t("aboutLkodPage.lKODAdministrationparagraph5")}
                            </li>
                        </ul>
                    </div>
                    <div className={styles["image-box"]}>
                        <Image src={LKOD_Administrace} alt={"LKOD_administrace"} width={636} height={441} objectFit="contain" />
                    </div>
                </div>
            </div>
            <div className={styles.container}>
                <section className={styles["container-links"]}>
                    <Card tag="div" className={styles["link-card"]}>
                        <AccessibilityIcon width={27} height={28} />
                        <div className={styles["link-card-text"]}>
                            <Heading tag="h3" type="h5">
                                {t("aboutLkodPage.accessibilityTitle")}
                            </Heading>
                            <a href="https://opendata.praha.eu/accessibility" target="_blank" rel="noreferrer">
                                <Text size="md">{t("aboutLkodPage.accessibilityDescription")}</Text>
                            </a>
                        </div>
                    </Card>
                    <Card tag="div" className={styles["link-card"]}>
                        <OpenSourceIcon width={30} height={28} />
                        <div className={styles["link-card-text"]}>
                            <Heading tag="h3" type="h5">
                                {t("aboutLkodPage.openSourceTitle")}
                            </Heading>
                            <a
                                href="https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/tree/master/demo"
                                target="_blank"
                                rel="noreferrer"
                            >
                                <Text size="md">{t("aboutLkodPage.openSourceDescription")}</Text>
                            </a>
                        </div>
                    </Card>
                    <Card tag="div" className={styles["link-card"]}>
                        <OfnIcon width={21} height={28} />
                        <div className={styles["link-card-text"]}>
                            <Heading tag="h3" type="h5">
                                {t("aboutLkodPage.ofnTitle")}
                            </Heading>
                            <a
                                href="https://ofn.gov.cz/rozhran%C3%AD-katalog%C5%AF-otev%C5%99en%C3%BDch-dat/2021-01-11/"
                                target="_blank"
                                rel="noreferrer"
                            >
                                <Text size="md">{t("aboutLkodPage.ofnDescription")}</Text>
                            </a>
                        </div>
                    </Card>
                </section>
            </div>
        </main>
    );
};

export const getServerSideProps = async () => {
    return {
        props: {
            metaData: {
                title: Meta.aboutPage.title,
                ogTitle: Meta.aboutPage.ogTitle,
                ogDescription: Meta.aboutPage.ogDescription,
                ogImage: Meta.aboutPage.ogImage,
                twitterImage: Meta.aboutPage.twitterImage,
            },
        },
    };
};

export default AboutLkod;
