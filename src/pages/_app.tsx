import "@/styles/reset.css";
import "@/styles/globals.scss";
import "../config"; // explicitly init config
import "../i18n";

import type { AppProps } from "next/app";
import { useRouter } from "next/router";
import PlausibleProvider from "next-plausible";
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import { IMetaData } from "@/components/HeadContainer";
import { Spinner } from "@/components/Spinner";
import ToastCta from "@/components/ToastCta";
import Layout from "@/layouts/Layout";
import { config } from "@/root/config";

import { DataCtxProvider } from "../store/dataContext";

interface CustomPageProps {
    metaData: IMetaData;
}

const MyApp = ({ Component, pageProps }: AppProps<CustomPageProps>) => {
    const metaData = pageProps.metaData;
    const [isLoading, setIsLoading] = useState(true);
    const router = useRouter();
    const { t } = useTranslation();

    useEffect(() => {
        router.isReady && setIsLoading(false);
    }, [router.isReady]);

    return (
        <PlausibleProvider domain={config.PUBLIC_URL.replace(/^https?:\/\//i, "")} trackOutboundLinks trackFileDownloads>
            <DataCtxProvider>
                <div id="modal-root"></div>
                <Layout metaData={metaData}>
                    {isLoading ? <Spinner /> : <Component {...pageProps} />}
                    <ToastCta
                        message={t("cta.ctaTitle")}
                        url="https://docs.google.com/forms/d/e/1FAIpQLSfKsMfTu60-pWHlYgKYUhu6DlZ5Te1eM18vEP7QihLzcraUaQ/viewform"
                    />
                </Layout>
            </DataCtxProvider>
        </PlausibleProvider>
    );
};

export default MyApp;
