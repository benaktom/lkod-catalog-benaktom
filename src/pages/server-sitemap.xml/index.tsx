import { GetServerSideProps } from "next";
import { getServerSideSitemap, ISitemapField } from "next-sitemap";

import { DatasetsItem } from "@/root/schema/dataset";
import { CatalogService } from "@/services/catalog-service";

const service = new CatalogService();

const getDataset = async () => {
    const { datasets } = await service.findDatasets({
        limit: 1000,
    });
    return datasets;
};

export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const siteUrl = "https://opendata.praha.eu";
    const data: DatasetsItem[] = await getDataset();
    const fields: ISitemapField[] = data?.map((data) => ({
        loc: `${siteUrl}/datasets/${encodeURIComponent(data.iri)}`,
        lastmod: new Date().toISOString(),
    }));

    return getServerSideSitemap(ctx, fields);
};

export default function Site() {
    //console
}
