/* eslint-disable react-hooks/exhaustive-deps */
import isDeepEqual from "fast-deep-equal/es6/react";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";

import { Breadcrumb } from "@/components/Breadcrumb";
import Button from "@/components/Button";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import { NoResult } from "@/components/NoResult";
import OrgIconFrame from "@/components/OrganizationIconFrame";
import Paginator from "@/components/Paginator/Paginator";
import { ResultLine } from "@/components/ResultLine";
import { Spinner } from "@/components/Spinner";
import useWindowSize, { ISize } from "@/hooks/useWindowSize";
import DatasetCard from "@/modules/datasets/DatasetCard";
import Filters from "@/modules/filters/Filters";
import FilterView from "@/modules/filters/FilterView";
import Meta from "@/root/metatags.json";
import { DatasetsItem } from "@/root/schema";
import { CatalogService } from "@/root/services/catalog-service";
import { IPublisherData } from "@/schema/common";
import { stringOrArr } from "@/utils/helpers";

import styles from "./OrganizationsDatasets.module.scss";

const service = new CatalogService();
const PER_PAGE = 6;

type Page = NextPage & {
    urlOrganization: string;
    count: number;
    datasets: DatasetsItem[];
    organizations: IPublisherData[];
    data: DatasetsItem[];
    org: IPublisherData;
};
const Organization = ({ count, datasets, org }: Page) => {
    const PAGE_LABEL = org.name;
    const { t } = useTranslation();
    const router = useRouter();

    const { page, publisherIri, themeIris, keywords, formatIris, search, perpage } = router.query;
    const [scrollY, setScrollY] = useState<number | undefined>(undefined);
    const size: ISize = useWindowSize();
    const [currentPage, setCurrentPage] = useState(1);
    const [isLoading, setisLoading] = useState(false);

    const showMoreRef = useRef<HTMLButtonElement>(null);
    const headingRef = useRef<HTMLDivElement>(null);

    const [perPage, setPerPage] = useState(PER_PAGE);

    const datasetsRef = useRef<DatasetsItem[]>(datasets);
    const keywordsRef = useRef(keywords);
    const themeIrisRef = useRef(themeIris);
    const formatIrisRef = useRef(formatIris);

    if (!isDeepEqual(datasetsRef.current, datasets)) {
        datasetsRef.current = datasets;
    }
    if (!isDeepEqual(keywordsRef.current, keywords)) {
        keywordsRef.current = keywords;
    }
    if (!isDeepEqual(themeIrisRef.current, themeIris)) {
        themeIrisRef.current = themeIris;
    }

    if (!isDeepEqual(formatIrisRef.current, formatIris)) {
        formatIrisRef.current = formatIris;
    }

    const refreshData = () => {
        setisLoading(true);
        router.replace(router.asPath);
    };

    const perPageHandler = () => {
        setScrollY(window.scrollY);
        if (perPage < count && perPage + PER_PAGE < count) {
            setPerPage((prev) => prev + PER_PAGE);
        } else setPerPage(count);
        if (perPage >= count) {
            setPerPage(PER_PAGE);
            headingRef.current?.scrollIntoView({ behavior: "smooth", block: "start" });
            setScrollY(undefined);
        }
    };

    useEffect(() => {
        refreshData();
        if (page) {
            setCurrentPage(+page);
        }
    }, [router.isReady, page, publisherIri, themeIrisRef.current, keywordsRef.current, formatIrisRef.current, search, perpage]);

    useEffect(() => {
        router.push({ query: { ...router.query, perpage: perPage } }, undefined, { shallow: true });
    }, [perPage]);

    useEffect(() => {
        if (perpage && +perpage > PER_PAGE) {
            scrollY && window.scrollTo({ top: scrollY });
        }
        setisLoading(false);
    }, [datasets, perpage, scrollY]);
    let datasetsList;

    if (isLoading) {
        datasetsList = <Spinner />;
    }

    if (!isLoading && datasetsRef.current.length > 0) {
        datasetsList = (
            <div className={styles["datasets-list"]}>
                {datasets?.map((ds: DatasetsItem) => {
                    return (
                        <DatasetCard
                            key={ds.iri}
                            iri={ds.iri}
                            title={ds.title}
                            description={ds.description}
                            publisher={ds.publisher}
                            formats={ds.formats}
                            themeNames={ds.themeNames}
                            themeImage={ds.themeImage}
                        />
                    );
                })}
            </div>
        );
    }
    return (
        <Container>
            <Breadcrumb
                labels={[
                    { label: t("navBar.organizations"), link: `/organizations` },
                    { label: org.name, link: "" },
                ]}
            />
            <Heading tag={`h1`} ref={headingRef}>
                {PAGE_LABEL}
            </Heading>
            <div className={styles["organization-container"]}>
                <FilterView orgPage={true} />
                <OrgIconFrame src={org.logo ?? null} alt={org.name} name={org.name} description={org.description} />
                <Filters label={t("filters.title")} publisherIri={org.iri} />

                <div className={styles["organization-datasets"]}>
                    <ResultLine result={count} orgPage={true} />
                    {+count === 0 && <NoResult />}
                    {datasetsList}
                </div>
                <div className={styles["organization-navigation"]}>
                    {count > PER_PAGE && (
                        <Button
                            color={`secondary`}
                            label={`Zobrazit ${perPage >= count ? "prvních" : "dalších"}  ${PER_PAGE}`}
                            onClick={perPageHandler}
                            ref={showMoreRef}
                        />
                    )}
                    <Paginator
                        totalCount={count}
                        itemsPerPage={perPage}
                        currentPage={currentPage}
                        setCurrentPage={setCurrentPage}
                        nItemsWithin={size.width && size.width > 768 ? (size.width > 1200 ? 3 : 2) : 1}
                    />
                </div>
            </div>
        </Container>
    );
};

export async function getServerSideProps(context: {
    params: { organization: string };
    query: {
        page?: string;
        publisherIri?: string;
        themeIris?: string | string[];
        keywords?: string | string[];
        formatIris?: string | string[];
        perpage?: string;
    };
}) {
    const organizations: IPublisherData[] = await service.getAllPublishersData();
    const urlOrganization: string = context.params.organization;

    const { themeIris, keywords, formatIris, page, perpage } = context.query;

    const org = organizations.find((org: IPublisherData) => org.slug === urlOrganization && org.count > 0);

    const filter = {
        publisherIri: org?.iri ? org.iri : "",
        themeIris: themeIris ? stringOrArr(themeIris) : [],
        keywords: keywords ? stringOrArr(keywords) : [],
        formatIris: formatIris ? stringOrArr(formatIris) : [],
    };

    const offset = page ? (+page - 1) * PER_PAGE : 0;

    const limit = perpage ? +perpage : PER_PAGE;

    const { count, datasets } = await service.findDatasets({
        limit,
        offset,
        filter,
    });
    if (!org) {
        return {
            notFound: true,
        };
    }
    const data = datasets.filter((data: DatasetsItem) => data.publisher === org.name);

    return {
        props: {
            urlOrganization,
            organizations,
            count,
            data,
            org,
            datasets,
            metaData: {
                title: Meta.datasetsPage.title,
                ogImage: Meta.datasetsPage.ogImage,
                twitterImage: Meta.datasetsPage.twitterImage,
            },
        },
    };
}

export default Organization;
