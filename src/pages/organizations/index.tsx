import type { NextPage } from "next";
import { useRouter } from "next/router";
import React, { FC, useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";

import { Breadcrumb } from "@/components/Breadcrumb";
import Button from "@/components/Button";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import Paginator from "@/components/Paginator/Paginator";
import { ResultLine } from "@/components/ResultLine";
import useWindowSize from "@/hooks/useWindowSize";
import { ISize } from "@/hooks/useWindowSize";
import { OrganizationCard } from "@/modules/organizations/OrganizationCard";
import { SearchForm } from "@/modules/search/SearchForm";
import Meta from "@/root/metatags.json";
import { CatalogService } from "@/root/services/catalog-service";
import { useDataCtx } from "@/root/store/dataContext";
import { IPublisherData } from "@/schema/common";
import styles from "@/styles/Organizations.module.scss";

const service = new CatalogService();

type Page = NextPage & {
    organizations: IPublisherData[];
};

const PAGE_LABEL = Meta.organizationsPage.title;

const PER_PAGE = 8;

const Organization: FC<Page> = ({ organizations }) => {
    const { t } = useTranslation();
    const router = useRouter();

    const { initialFilter, setCtxFilter } = useDataCtx();
    const size: ISize = useWindowSize();
    const [currentPage, setCurrentPage] = useState(router.query.page ? +router.query.page : 1);
    const [perPage, setPerPage] = useState(PER_PAGE);

    const headingRef = useRef<HTMLDivElement>(null);

    const perPageHandler = () => {
        if (perPage < organizations.length && perPage + PER_PAGE < organizations.length) {
            setPerPage((prev) => prev + PER_PAGE);
        } else setPerPage(organizations.length);
        if (perPage >= organizations.length) {
            setPerPage(PER_PAGE);
            headingRef.current?.scrollIntoView({ behavior: "smooth", block: "start" });
        }
    };

    const pushOrgHandler = (publisher: IPublisherData) => {
        if (publisher.count > 0) {
            router.push({ pathname: `/organizations/${publisher.slug}` }, undefined, {
                shallow: false,
            }),
                setCtxFilter({ ...initialFilter, publisherIri: publisher.iri });
        } else {
            router.push({ pathname: "/organizations" }, undefined, { shallow: true });
        }
    };

    useEffect(() => {
        if (router.query.page) {
            headingRef.current?.scrollIntoView({ behavior: "smooth", block: "start" });
        }
    }, [currentPage, router.query.page]);

    return (
        <>
            <Container>
                <Breadcrumb labels={[{ label: PAGE_LABEL, link: "" }]} />
                <Heading tag={`h1`} ref={headingRef}>
                    {PAGE_LABEL}
                </Heading>
                <SearchForm label={t("searchString.title")} fullWidth />
                <div className={styles["organizations-container"]}>
                    <ResultLine result={organizations.length} organization />
                    <div className={styles["organization-list"]}>
                        {organizations.slice((currentPage - 1) * PER_PAGE, currentPage * perPage).map((org, i) => {
                            return (
                                <OrganizationCard
                                    key={i}
                                    label={org.name}
                                    logoUrl={org.logo ?? ""}
                                    description={org.description}
                                    count={org.count}
                                    onClick={() => pushOrgHandler(org)}
                                />
                            );
                        })}
                    </div>
                    <div className={styles.navigation}>
                        <Button
                            color={`secondary`}
                            label={`Zobrazit ${perPage >= organizations.length ? "prvních" : "dalších"}  ${PER_PAGE}`}
                            onClick={perPageHandler}
                        />
                        <Paginator
                            totalCount={organizations.length}
                            itemsPerPage={perPage}
                            currentPage={currentPage}
                            setCurrentPage={setCurrentPage}
                            nItemsWithin={size.width && size.width > 768 ? (size.width > 1200 ? 5 : 4) : 1}
                        />
                    </div>
                </div>
            </Container>
        </>
    );
};

export const getServerSideProps = async () => {
    const organizations = await service.getAllPublishersData();

    return {
        props: {
            organizations,
            metaData: {
                title: Meta.organizationsPage.title,
                ogImage: Meta.organizationsPage.ogImage,
                twitterImage: Meta.organizationsPage.twitterImage,
            },
        },
    };
};

export default Organization;
