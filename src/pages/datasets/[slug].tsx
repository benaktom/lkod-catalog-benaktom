import type { NextPage } from "next";
import { useRouter } from "next/router";
import { usePlausible } from "next-plausible";
import React, { FC, useState } from "react";
import { useTranslation } from "react-i18next";

import { Breadcrumb } from "@/components/Breadcrumb";
import Button from "@/components/Button";
import { Chip } from "@/components/Chip";
import Container from "@/components/Container";
import { IMetaData } from "@/components/HeadContainer";
import { Heading } from "@/components/Heading";
import IconExtSource from "@/components/IconExtSource";
import { FileCleanIcon } from "@/components/icons/FileCleanIcon";
import { InfoIcon } from "@/components/icons/InfoIcon";
import { Spinner } from "@/components/Spinner";
import DatasetOrganizationCard from "@/modules/datasets/DatasetOrganizationCard";
import { DistributionDialog } from "@/modules/datasets/DistributionDialog";
import { MetadataCard } from "@/modules/datasets/MetadatCard";
import ServiceDialog from "@/modules/datasets/ServiceDialog";
import { Dataset } from "@/schema/dataset";
import { Distribution, DistributionService } from "@/schema/distribution";
import { CatalogService } from "@/services/catalog-service";

import styles from "./DatasetDetail.module.scss";

const service = new CatalogService();

type Page = NextPage & {
    data: Dataset;
    distributionFiles: Distribution[];
    distributionServices: DistributionService[];
};

const DatasetPage: FC<Page> = ({ data, distributionFiles, distributionServices }) => {
    const { t } = useTranslation();
    const router = useRouter();
    const [distribution, setDistribution] = useState<Distribution | null>(null);
    const [distributionService, setDistributionService] = useState<DistributionService | null>(null);
    const plausible = usePlausible();

    if (router.isFallback || !data || !distributionFiles || !distributionServices) {
        return <Spinner />;
    }

    const downloadFile = (url: string) => {
        window.location.href = url + "?download=true";
    };

    return (
        <Container>
            <Breadcrumb
                labels={[
                    { label: t("navBar.datasets"), link: `/datasets` },
                    { label: data?.title, link: "" },
                ]}
            />
            <Heading tag={`h1`} type={`h2`}>
                {data?.title}
            </Heading>
            <p className={styles.description}>{data.description}</p>
            <section className={styles["title-container"]}>
                {data.themes.map((theme, i) => {
                    return (
                        <div key={i} className={styles["title-icon"]}>
                            {theme.image ? <IconExtSource src={theme.image} alt={theme.title} /> : false}
                            <span className={i > 0 ? styles["dashed-span"] : ""}>{theme.title}</span>
                            {data.publisher ? <span>{data.publisher.title}</span> : false}
                        </div>
                    );
                })}
            </section>
            <div className={data.publisher ? styles["detail-container"] : styles["detail-container-noorg"]}>
                {data.publisher ? (
                    <DatasetOrganizationCard
                        label={data.publisher.title}
                        logoUrl={data.publisher.logo ?? ""}
                        organizationUrl={data.publisher.iri}
                    />
                ) : (
                    false
                )}
                {distributionFiles.length > 0 && (
                    <section className={styles["dataset-files"]}>
                        <Heading tag={`h4`}>{t("distributionFilesHeading")}</Heading>
                        {distributionFiles.map((file, i) => {
                            return (
                                <div className={styles["file-card"]} key={i}>
                                    <div>
                                        <div className={styles["file-card__row"]}>
                                            <div className={styles["file-card__icon"]}>
                                                <FileCleanIcon width={"2rem"} />
                                            </div>
                                            <div className={styles["file-card__heading"]}>
                                                {file.title ? (
                                                    <Heading tag={`h5`} type={`h6`} className={styles["file-card__label"]}>
                                                        {file.title}
                                                    </Heading>
                                                ) : (
                                                    false
                                                )}
                                                <button
                                                    className={styles["file-card__info"]}
                                                    onClick={() => setDistribution(file)}
                                                    type="button"
                                                >
                                                    <InfoIcon color={`primary`} width="1.4rem" height="1.4rem" />
                                                    <span className={styles["file-card__info-text"]}>
                                                        {t("distributionFileInfo")}
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                        <p className={styles["file-type"]}>
                                            {file.format.title ?? t("distributionFileTypeUnknown")}
                                        </p>
                                    </div>
                                    <div className={styles["file-card__col"]}>
                                        <Button
                                            className={styles["file-card__download"]}
                                            label={t("buttons.download")}
                                            color={`primary`}
                                            type="button"
                                            onClick={() => {
                                                downloadFile(file.downloadUrl);
                                                plausible("trackFileDownloads: DownloadFile", {
                                                    props: {
                                                        fileName: file.title,
                                                        fileFormat: file.format.title,
                                                        dataset: data?.title,
                                                        file:
                                                            "Datový soubor: " +
                                                            file.title +
                                                            ", format: " +
                                                            file.format.title +
                                                            ",  Datová sada: " +
                                                            data?.title,
                                                    },
                                                });
                                            }}
                                        />
                                    </div>
                                </div>
                            );
                        })}
                    </section>
                )}
                {distributionServices.length > 0 && (
                    <section className={`${styles["dataset-files"]} ${styles["dataset-services"]}`}>
                        <Heading tag={`h4`}>{t("heading.dataservices")}</Heading>
                        {distributionServices.map((service, i) => {
                            return (
                                <div className={styles["file-card"]} key={i}>
                                    <div className={styles["file-card__row"]}>
                                        <FileCleanIcon />
                                        <div className={styles["file-card__heading"]}>
                                            <Heading tag={`h5`} type={`h6`} className={styles["file-card__label"]}>
                                                {service.title}
                                            </Heading>
                                            <button
                                                className={styles["file-card__info"]}
                                                onClick={() => setDistributionService(service)}
                                            >
                                                <InfoIcon color={`primary`} width="1.4rem" height="1.4rem" />
                                                <p>{t("distributionServiceInfo")}</p>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            );
                        })}
                    </section>
                )}
                <section className={styles.metadata}>
                    <Heading tag={`h4`}>{t("heading.metadata")}</Heading>
                    <MetadataCard label={t("metadataCard.keywords")}>
                        <div className={styles["metadata__keywords"]}>
                            {data.keywords.map((kword, i) => {
                                return <Chip key={i} label={kword} />;
                            })}
                        </div>
                    </MetadataCard>
                    {data.documentation && (
                        <MetadataCard label={t("metadataCard.documentlink")}>
                            <a
                                href={`${decodeURIComponent(data.documentation ?? "")}`}
                                className={styles["metadata__link"]}
                                target="_blank"
                                rel="noreferrer"
                            >
                                {decodeURIComponent(data.documentation ?? "")}
                            </a>
                        </MetadataCard>
                    )}
                    {data.specification && (
                        <MetadataCard label={t("metadataCard.specificationlink")}>
                            <a
                                href={`${decodeURIComponent(data.specification ?? "")}`}
                                className={styles["metadata__link"]}
                                target="_blank"
                                rel="noreferrer"
                            >
                                {decodeURIComponent(data.specification ?? "")}
                            </a>
                        </MetadataCard>
                    )}
                    {data.contactPoint && (
                        <MetadataCard label={t("metadataCard.contacts")}>
                            <a href={data.contactPoint.email}>{data.contactPoint.name}</a>
                        </MetadataCard>
                    )}
                    {data.themes.length > 0 && (
                        <MetadataCard label={t("metadataCard.theme")}>
                            <div className={styles["metadata__themes"]}>
                                {data.themes.map((theme, i) => {
                                    return (
                                        <div key={i} className={styles["metadata__theme"]}>
                                            {theme.image ? <IconExtSource src={theme.image} alt={theme.title} /> : false}
                                            <p>{theme.title}</p>
                                        </div>
                                    );
                                })}
                            </div>
                        </MetadataCard>
                    )}
                    {data.accrualPeriodicity && (
                        <MetadataCard label={t("metadataCard.updatefrequency")}>
                            <p>{data.accrualPeriodicity}</p>
                        </MetadataCard>
                    )}
                    {data.temporalFrom && data.temporalTo && (
                        <MetadataCard label={t("metadataCard.time-coverage")}>
                            <div className={styles["metadata__temporal-line"]}>
                                <p>{data.temporalFrom}</p>
                                <p>{data.temporalTo}</p>
                            </div>
                        </MetadataCard>
                    )}
                    {data.temporalResolution && (
                        <MetadataCard label={t("metadataCard.accruals")}>
                            <p>{data.temporalResolution}</p>
                        </MetadataCard>
                    )}
                    {data.spatialResolutionInMeters && (
                        <MetadataCard label={t("metadataCard.resolutioninmeters")}>
                            <p>{data.spatialResolutionInMeters}</p>
                        </MetadataCard>
                    )}
                    {data.spatial && (
                        <MetadataCard label={t("metadataCard.geographicarea")}>
                            <div className={styles["metadata__links"]}>
                                {data.spatial.map((link, i) => {
                                    return (
                                        <a
                                            key={i}
                                            href={link}
                                            className={styles["metadata__link"]}
                                            target="_blank"
                                            rel="noreferrer"
                                        >
                                            {link}
                                        </a>
                                    );
                                })}
                            </div>
                        </MetadataCard>
                    )}
                    {data.eurovocThemes.length > 0 && (
                        <MetadataCard label={t("metadataCard.classification")}>
                            <div className={styles["metadata__links"]}>
                                {data.eurovocThemes.map((link, i) => {
                                    return (
                                        <a
                                            key={i}
                                            href={link}
                                            className={styles["metadata__link"]}
                                            target="_blank"
                                            rel="noreferrer"
                                        >
                                            {link}
                                        </a>
                                    );
                                })}
                            </div>
                        </MetadataCard>
                    )}
                </section>
            </div>
            {distribution && (
                <DistributionDialog
                    closeDialog={() => setDistribution(null)}
                    distribution={distribution}
                    isOpen={!!distribution}
                />
            )}
            {distributionService && (
                <ServiceDialog
                    closeDialog={() => setDistributionService(null)}
                    distributionService={distributionService}
                    isOpen={!!distributionService}
                />
            )}
        </Container>
    );
};

export async function getServerSideProps(context: { params: { slug: string } }) {
    const iri = decodeURIComponent(context.params.slug);
    const data = await service.getDataset(iri);

    if (!data) {
        return {
            notFound: true,
        };
    }

    const getDistributionFiles = async () => {
        if (data.distributions.files.length > 0) {
            const promises = data.distributions.files.map(async (file) => {
                const res = await service.getDistribution(file.distributionIri);
                return res;
            });
            return await Promise.all(promises);
        }
    };
    const distributionFiles = await getDistributionFiles();

    const getDistributionServices = async () => {
        if (data.distributions.services.length > 0) {
            const promises = data.distributions.services.map(async (serv) => {
                const res = await service.getDistributionService(serv.accessServiceIri);
                return res;
            });
            return await Promise.all(promises);
        }
    };
    const distributionServices = await getDistributionServices();

    return {
        props: {
            data,
            distributionFiles: distributionFiles ? distributionFiles : [],
            distributionServices: distributionServices ? distributionServices : [],
            page: data.title,
            metaData: data as IMetaData,
        },
    };
}

export default DatasetPage;
