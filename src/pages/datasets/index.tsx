/* eslint-disable react-hooks/exhaustive-deps */
import isDeepEqual from "fast-deep-equal/es6/react";
import type { NextPage } from "next";
import { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";

import { Breadcrumb } from "@/components/Breadcrumb";
import Button from "@/components/Button";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import { NoResult } from "@/components/NoResult";
import Paginator from "@/components/Paginator/Paginator";
import { ResultLine } from "@/components/ResultLine";
import { Spinner } from "@/components/Spinner";
import useWindowSize, { ISize } from "@/hooks/useWindowSize";
import DatasetCard from "@/modules/datasets/DatasetCard";
import Filters from "@/modules/filters/Filters";
import FilterView from "@/modules/filters/FilterView";
import { SearchForm } from "@/modules/search/SearchForm";
import Meta from "@/root/metatags.json";
import { DatasetsItem } from "@/schema/dataset";
import { CatalogService } from "@/services/catalog-service";
import styles from "@/styles/Datasets.module.scss";
import { stringOrArr } from "@/utils/helpers";

const service = new CatalogService();

type Page = NextPage & {
    count: number;
    datasets: DatasetsItem[];
};

const PER_PAGE = 6;

const PAGE_LABEL = Meta.datasetsPage.title;

const Datasets = ({ count, datasets }: Page) => {
    const { t } = useTranslation();
    const router = useRouter();
    const { page, publisherIri, themeIris, keywords, formatIris, search, perpage } = router.query;

    const [scrollY, setScrollY] = useState<number | undefined>(undefined);
    const size: ISize = useWindowSize();
    const [currentPage, setCurrentPage] = useState(1);
    const [isLoading, setIsLoading] = useState(false);

    const showMoreRef = useRef<HTMLButtonElement>(null);
    const searchFormRef = useRef<HTMLDivElement>(null);
    const headingRef = useRef<HTMLDivElement>(null);

    const [perPage, setPerPage] = useState(PER_PAGE);

    const datasetsRef = useRef<DatasetsItem[]>(datasets);
    const keywordsRef = useRef(keywords);
    const themeIrisRef = useRef(themeIris);
    const formatIrisRef = useRef(formatIris);

    if (!isDeepEqual(datasetsRef.current, datasets)) {
        datasetsRef.current = datasets;
    }
    if (!isDeepEqual(keywordsRef.current, keywords)) {
        keywordsRef.current = keywords;
    }
    if (!isDeepEqual(themeIrisRef.current, themeIris)) {
        themeIrisRef.current = themeIris;
    }

    if (!isDeepEqual(formatIrisRef.current, formatIris)) {
        formatIrisRef.current = formatIris;
    }

    const refreshData = () => {
        setIsLoading(true);
        router.replace(router.asPath);
    };

    const perPageHandler = () => {
        setScrollY(window.scrollY);
        if (perPage < count && perPage + PER_PAGE < count) {
            setPerPage((prev) => prev + PER_PAGE);
        } else setPerPage(count);
        if (perPage >= count) {
            setPerPage(PER_PAGE);
            headingRef.current?.scrollIntoView({ behavior: "smooth", block: "start" });
            setScrollY(undefined);
        }
    };

    useEffect(() => {
        refreshData();
        if (page) {
            setCurrentPage(+page);
        }
    }, [router.isReady, page, publisherIri, themeIrisRef.current, keywordsRef.current, formatIrisRef.current, search, perpage]);

    useEffect(() => {
        router.push({ query: { ...router.query, perpage: perPage } }, undefined, { shallow: true });
    }, [perPage]);

    useEffect(() => {
        if (perpage && +perpage > PER_PAGE) {
            scrollY && window.scrollTo({ top: scrollY });
        }
        setIsLoading(false);
    }, [datasets, perpage]);
    let datasetsList;

    if (isLoading) {
        datasetsList = <Spinner />;
    }

    if (!isLoading && datasetsRef.current.length > 0) {
        datasetsList = (
            <div className={styles["datasets-list"]}>
                {datasets.map((ds: DatasetsItem) => {
                    return (
                        <DatasetCard
                            key={ds.iri}
                            iri={ds.iri}
                            title={ds.title}
                            description={ds.description}
                            publisher={ds.publisher}
                            formats={ds.formats}
                            themeNames={ds.themeNames}
                            themeImage={ds.themeImage}
                        />
                    );
                })}
            </div>
        );
    }

    return (
        <Container>
            <Breadcrumb labels={[{ label: PAGE_LABEL, link: "" }]} />
            <Heading tag={`h1`} ref={headingRef}>
                {PAGE_LABEL}
            </Heading>
            <SearchForm label={t("searchString.title")} fullWidth ref={searchFormRef} />
            <div className={styles["datasets-container"]}>
                <Filters label={t("filters.title")} />
                <FilterView />
                <ResultLine result={count} />
                {+count === 0 && <NoResult />}
                {datasetsList}
                <div className={styles["datasets-navigation"]}>
                    {count > PER_PAGE && (
                        <Button
                            color={`secondary`}
                            label={`Zobrazit ${perPage >= count ? "prvních" : "dalších"}  ${PER_PAGE}`}
                            onClick={perPageHandler}
                            ref={showMoreRef}
                        />
                    )}
                    <Paginator
                        totalCount={count}
                        itemsPerPage={perPage}
                        currentPage={currentPage}
                        setCurrentPage={setCurrentPage}
                        nItemsWithin={size.width && size.width > 768 ? (size.width > 1200 ? 3 : 2) : 1}
                    />
                </div>
            </div>
        </Container>
    );
};

export async function getServerSideProps(context: {
    query: {
        page?: string;
        publisherIri?: string;
        themeIris?: string | string[];
        keywords?: string | string[];
        formatIris?: string | string[];
        search?: string;
        perpage?: string;
    };
}) {
    const { publisherIri, themeIris, keywords, formatIris, search, page, perpage } = context.query;

    const filter = {
        publisherIri: publisherIri ? publisherIri : "",
        themeIris: themeIris ? stringOrArr(themeIris) : [],
        keywords: keywords ? stringOrArr(keywords) : [],
        formatIris: formatIris ? stringOrArr(formatIris) : [],
    };

    const searchString = search ? search : "";

    const offset = page ? (+page - 1) * PER_PAGE : 0;

    const limit = perpage ? +perpage : PER_PAGE;

    const { count, datasets } = await service.findDatasets({
        limit,
        offset,
        filter,
        searchString,
    });
    return {
        props: {
            count,
            datasets,
            metaData: {
                title: Meta.datasetsPage.title,
                ogImage: Meta.datasetsPage.ogImage,
                twitterImage: Meta.datasetsPage.twitterImage,
            },
        },
    };
}

export default Datasets;
