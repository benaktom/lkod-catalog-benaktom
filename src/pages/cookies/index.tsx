import type { NextPage } from "next";
import React from "react";
import { Trans, useTranslation } from "react-i18next";

import { Breadcrumb } from "@/components/Breadcrumb";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import Meta from "@/root/metatags.json";
import styles from "@/styles/Cookies.module.scss";

const PAGE_LABEL = Meta.privacyPolicyPage.title;

const Project: NextPage = () => {
    const { t } = useTranslation();

    return (
        <Container>
            <Breadcrumb labels={[{ label: PAGE_LABEL, link: "" }]} />
            <Heading tag={`h1`}>{t("privacyPolicyPage.pageTitle")}</Heading>
            <section className={styles["text"]}>
                <p>
                    <Trans
                        i18nKey="privacyPolicyPage.privacyPolicyDescription"
                        t={t}
                        components={{
                            link1: <a href="https://opendata.praha.eu" target="_blank" rel="noreferrer" />,
                        }}
                    />
                </p>
                <p>
                    <Trans
                        i18nKey="privacyPolicyPage.MoreInformation"
                        t={t}
                        components={{ link1: <a href="https://plausible.io/data-policy" target="_blank" rel="noreferrer" /> }}
                    />
                </p>
            </section>
        </Container>
    );
};

export const getServerSideProps = async () => {
    return {
        props: {
            metaData: {
                title: Meta.privacyPolicyPage.title,
            },
        },
    };
};

export default Project;
