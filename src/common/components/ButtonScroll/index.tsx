import React, { ButtonHTMLAttributes, DetailedHTMLProps, FC } from "react";

import { ChevronIcon } from "@/components/icons/ChevronIcon";

import styles from "./ButtonScroll.module.scss";

type Props = DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> & {
    direction: "left" | "right";
    ariaLabel: string;
};

export const ButtonScroll: FC<Props> = ({ onClick, direction, ariaLabel }) => {
    return (
        <button type="button" className={styles["button-scroll"]} onClick={onClick} aria-label={ariaLabel}>
            <ChevronIcon direction={direction} color={`tertiary`} />
        </button>
    );
};
