import Image from "next/image";
import React from "react";
import { useTranslation } from "react-i18next";

import ColorLink from "@/components/ColorLink";
import mainBackgroundImage from "@/images/mainBackgroundImage.png";
import { config } from "@/root/config";

import { Heading } from "../Heading";
import styles from "./ProjectInfo.module.scss";

export const ProjectInfo = () => {
    const { t } = useTranslation();

    return (
        <section className={styles["project-info"]}>
            <Image
                src={mainBackgroundImage}
                alt={`lkod background`}
                layout="fill"
                objectFit="cover"
                objectPosition="50% 10%"
                quality={70}
            />
            <div className={styles["text-field"]}>
                <Heading tag={`h2`}>{t("navBar.aboutCatalog")}</Heading>
                <p>{t("aboutCatalogText")}</p>
                {config.SHOW_PROJECT_PAGE !== "false" && (
                    <ColorLink linkText={t("colorLink.showMore")} linkUrl="/project" start direction={`right`} />
                )}
            </div>
        </section>
    );
};
