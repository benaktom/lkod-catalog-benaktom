import Image from "next/image";
import { FC } from "react";

import searchIcon from "@/images/search.svg";

import styles from "./SearchInputSelf.module.scss";

type SearchInputProps = {
    id?: string;
    name: string;
    placeholder: string;
    onInput?: () => void;
    onBlur?: () => void;
    onClick: () => void;
    fullWidth?: boolean;
};

const SearchInputSelf: FC<SearchInputProps> = ({ id, name, onBlur, onClick, onInput, placeholder, fullWidth }) => {
    return (
        <div className={styles["search-wrapper"]}>
            <input
                className={`${styles["search-input"]} ${fullWidth ? styles.full : ""}`}
                type="search"
                id={id}
                name={name}
                placeholder={placeholder}
                onInput={onInput}
                onBlur={onBlur}
            />
            <div className={styles["icon-frame"]}>
                <Image src={searchIcon} alt="Search" onClick={onClick} />
            </div>
        </div>
    );
};

export default SearchInputSelf;
