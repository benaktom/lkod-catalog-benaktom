import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import { ButtonReset } from "../ButtonReset";
import ArrowIcon from "../icons/ArrowIcon";
import CTAButtonIcon from "../icons/CTAButtonIcon";
import styles from "./ToastCta.module.scss";

type ToastProps = {
    message: string;
    url?: string;
};

const ToastCta = ({ message, url }: ToastProps) => {
    const { t } = useTranslation();

    const [isVisible, setIsVisible] = useState(false);
    const [hasBeenShown, setHasBeenShown] = useState(
        typeof window !== "undefined" ? localStorage.getItem("hasBeenShown") : false
    );

    useEffect(() => {
        if (!hasBeenShown) {
            setIsVisible(true);
        }
    }, [hasBeenShown]);

    const handleClose = () => {
        setIsVisible(false);
        setHasBeenShown(true);
        localStorage.setItem("hasBeenShown", true.toString());
    };

    return (
        <>
            {isVisible && (
                <div className={styles.toast}>
                    <div className={styles["toast__header"]}>
                        <a href={url} target="_blank" rel="noreferrer" onClick={handleClose}>
                            <span className={styles["toast__header-content"]}>
                                <CTAButtonIcon color={`#5296D5`} width={22} height={22} />
                                <span className={styles["toast__header-message"]}>{message}</span>
                                <ArrowIcon />
                            </span>
                        </a>
                        <ButtonReset
                            label={t("buttons.close")}
                            hideLabel
                            onClick={() => handleClose()}
                            color="gray-dark"
                            className={styles["toast__closeButton"]}
                        />
                    </div>
                    <div className={styles["toast__ctaDescription"]}>{t("cta.ctaDescription")}</div>
                </div>
            )}
        </>
    );
};

export default ToastCta;
