import Link from "next/link";
import React, { FC, useState } from "react";
import { useTranslation } from "react-i18next";

import { BurgerIcon } from "@/components/icons/BurgerIcon";
import { CloseIcon } from "@/components/icons/CloseIcon";
import { useOrganizations } from "@/hooks/useOrganizations";
import useWindowSize from "@/hooks/useWindowSize";
import { ISize } from "@/hooks/useWindowSize";
import { config } from "@/root/config";
import { useDataCtx } from "@/root/store/dataContext";

import styles from "./NavBar.module.scss";

export const NavBar: FC = () => {
    const { t } = useTranslation();
    const size: ISize = useWindowSize();

    const { initialFilter, setCtxFilter, setCtxSearchString } = useDataCtx();
    const { publishers, isLoading } = useOrganizations();

    const showOrgs = !isLoading && publishers && publishers.length > 0;

    const [open, setOpen] = useState(false);

    const toggleHandler = () => {
        setOpen(!open);
    };

    const resetFiltersAndSearch = () => {
        setCtxSearchString("");
        setCtxFilter({ ...initialFilter });
        toggleHandler();
    };

    return (
        <nav className={styles.navbar}>
            {size.width && size.width < 991 && (
                <button className={styles["navbar__button"]} onClick={toggleHandler} aria-label={`Menu`} type="button">
                    {open ? <CloseIcon color={`tertiary`} /> : <BurgerIcon color={"tertiary"} />}

                    <span className={styles["navbar__title"]}>MENU</span>
                </button>
            )}
            <div className={`${styles["navbar__menu"]} ${open ? styles["open"] : ""}`}>
                <ul>
                    <li>
                        <Link href={{ pathname: "/datasets", query: { ...initialFilter } }}>
                            <a onClick={resetFiltersAndSearch}>{t("navBar.datasets")}</a>
                        </Link>
                    </li>
                    {showOrgs ? (
                        <li>
                            <Link href={`/organizations`}>
                                <a onClick={toggleHandler}>{t("navBar.organizations")}</a>
                            </Link>
                        </li>
                    ) : (
                        false
                    )}

                    {config.SHOW_PROJECT_PAGE !== "false" && (
                        <li>
                            <Link href={`/project`}>
                                <a onClick={toggleHandler}>{t("navBar.aboutCatalog")}</a>
                            </Link>
                        </li>
                    )}

                    {config.SHOW_ABOUTLKOD_PAGE !== "false" && (
                        <li>
                            <Link href={`/about-lkod`}>
                                <a onClick={toggleHandler}>{t("navBar.aboutLKOD")}</a>
                            </Link>
                        </li>
                    )}
                </ul>
            </div>
        </nav>
    );
};
