import React, { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

import styles from "./IconFills.module.scss";

export const InfoIcon = ({ color, width = "1rem", height = "1rem", ...props }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        fill="none"
        viewBox={`0 0 20 20`}
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <g filter="url(#a)" transform="matrix(1.2 0 0 1.2 -1.8 -2.6)">
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M8.946 3.452a7.28 7.28 0 0 0-7.28 7.28 7.279 7.279 0 0 0 7.28 7.279 7.28 7.28 0 0 0 
                0-14.559Zm1.529 3.984a1.436 1.436 0 1 0-2.872 0 1.436 1.436 0 0 0 2.872 0Zm.068 7.655a.41.41 0 0 
                0 .411-.41v-.82a.41.41 0 0 0-.411-.41h-.41v-3.418a.41.41 0 0 0-.409-.41H7.536a.41.41 0 0 0-.411.41v.821a.41.41 
                0 0 0 .411.409h.41v2.188h-.41a.41.41 0 0 0-.411.41v.82c0 .226.184.41.411.41h3.007Z"
            />
        </g>
        <defs>
            <filter
                id="a"
                x={1.666}
                y={2.167}
                width={16.666}
                height={20.667}
                filterUnits="userSpaceOnUse"
                colorInterpolationFilters="sRGB"
            >
                <feFlood floodOpacity={0} result="BackgroundImageFix" />
                <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
                <feColorMatrix in="SourceAlpha" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                <feOffset dy={4} />
                <feGaussianBlur stdDeviation={2} />
                <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
                <feColorMatrix values="0 0 0 0 0 0 0 0 0 0.2 0 0 0 0 0.4 0 0 0 0.1 0" />
                <feBlend in2="shape" result="effect1_innerShadow_363_10929" />
            </filter>
        </defs>
    </svg>
);
