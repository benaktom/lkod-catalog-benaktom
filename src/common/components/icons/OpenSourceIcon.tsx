import * as React from "react";
import { SVGProps } from "react";

const OpenSourceIcon = ({ width = "32", height = "26", ...props }: SVGProps<SVGSVGElement>) => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} fill="none" viewBox="0 0 32 26" {...props}>
            <g filter="url(#filter0_i_4300_14545)">
                <path
                    fill="#ED2232"
                    // eslint-disable-next-line max-len
                    d="M20.406 2.922l-6 21c-.234.797-1.031 1.265-1.828 1.031-.797-.234-1.265-1.031-1.031-1.828l6-21c.234-.797 1.031-1.266 1.828-1.031.797.234 1.266 1.031 1.031 1.828zm4.875 3.797l5.25 5.25c.61.562.61 1.547 0 2.11l-5.25 5.25c-.562.608-1.547.608-2.11 0a1.445 1.445 0 010-2.11L27.345 13l-4.172-4.172a1.445 1.445 0 010-2.11 1.445 1.445 0 012.11 0zm-16.5 2.11L4.61 13l4.172 4.219c.61.562.61 1.547 0 2.11-.562.608-1.547.608-2.11 0l-5.25-5.25a1.445 1.445 0 010-2.11l5.25-5.25a1.445 1.445 0 012.11 0c.61.562.61 1.547 0 2.11z"
                ></path>
            </g>
            <defs>
                <filter
                    id="filter0_i_4300_14545"
                    width="30.328"
                    height="28.328"
                    x="0.813"
                    y="0.859"
                    colorInterpolationFilters="sRGB"
                    filterUnits="userSpaceOnUse"
                >
                    <feFlood floodOpacity="0" result="BackgroundImageFix"></feFlood>
                    <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape"></feBlend>
                    <feColorMatrix
                        in="SourceAlpha"
                        result="hardAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    ></feColorMatrix>
                    <feOffset dy="4"></feOffset>
                    <feGaussianBlur stdDeviation="2"></feGaussianBlur>
                    <feComposite in2="hardAlpha" k2="-1" k3="1" operator="arithmetic"></feComposite>
                    <feColorMatrix values="0 0 0 0 0 0 0 0 0 0.2 0 0 0 0 0.4 0 0 0 0.1 0"></feColorMatrix>
                    <feBlend in2="shape" result="effect1_innerShadow_4300_14545"></feBlend>
                </filter>
            </defs>
        </svg>
    );
};

export default OpenSourceIcon;
