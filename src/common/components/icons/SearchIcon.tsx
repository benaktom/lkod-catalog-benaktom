import React, { SVGProps } from "react";

export const SearchIcon = ({ width = "2rem", height = "2rem", ...props }: SVGProps<SVGSVGElement>) => {
    return (
        <svg
            style={{ width: width, height: height }}
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 32 32"
            {...props}
        >
            <g filter="url(#filter0_i_363_14722)">
                <path
                    fill="url(#paint0_linear_363_14722)"
                    fillOpacity="0.4"
                    fillRule="evenodd"
                    d="M14.4 0c7.953 0 14.4 6.447 14.4 14.4 0 3.4-1.178 6.524-3.148 8.987l5.88 5.882a1.6 1.6 0 
                    01-2.113 2.396l-.15-.134-5.881-5.88A14.34 14.34 0 0114.4 28.8C6.447 28.8 0 22.353 0 14.4S6.447 
                    0 14.4 0zm0 3.2C8.214 3.2 3.2 8.214 3.2 14.4c0 6.186 5.014 11.2 11.2 11.2 6.186 0 11.2-5.014 
                    11.2-11.2 0-6.186-5.014-11.2-11.2-11.2z"
                    clipRule="evenodd"
                ></path>
            </g>
            <defs>
                <filter
                    id="filter0_i_363_14722"
                    width="32"
                    height="36"
                    x="0"
                    y="0"
                    colorInterpolationFilters="sRGB"
                    filterUnits="userSpaceOnUse"
                >
                    <feFlood floodOpacity="0" result="BackgroundImageFix"></feFlood>
                    <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape"></feBlend>
                    <feColorMatrix
                        in="SourceAlpha"
                        result="hardAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    ></feColorMatrix>
                    <feOffset dy="4"></feOffset>
                    <feGaussianBlur stdDeviation="2"></feGaussianBlur>
                    <feComposite in2="hardAlpha" k2="-1" k3="1" operator="arithmetic"></feComposite>
                    <feColorMatrix values="0 0 0 0 0 0 0 0 0 0.2 0 0 0 0 0.4 0 0 0 0.1 0"></feColorMatrix>
                    <feBlend in2="shape" result="effect1_innerShadow_363_14722"></feBlend>
                </filter>
                <linearGradient
                    id="paint0_linear_363_14722"
                    x1="32.08"
                    x2="0.08"
                    y1="0"
                    y2="32.08"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#96AAB7"></stop>
                    <stop offset="1" stopColor="#728896"></stop>
                </linearGradient>
            </defs>
        </svg>
    );
};
