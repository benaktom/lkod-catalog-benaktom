import React, { SVGProps } from "react";
export const NoResultIcon = ({ width = "4rem", height = "4rem", ...restProps }: SVGProps<SVGSVGElement>) => (
    <svg
        style={{ width: width, height: height }}
        viewBox="0 0 64 64"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <g filter="url(#filter0_i_363_12374)">
            <path
                // eslint-disable-next-line max-len
                d="M0 32C0 14.375 14.25 0 32 0C49.625 0 64 14.375 64 32C64 49.75 49.625 64 32 64C14.25 64 0 49.75 0 32ZM19.875 48.625C21.375 43.75 26.125 40 32 40C37.75 40 42.5 43.75 44 48.625C44.375 49.75 45.5 50.25 46.5 50C47.625 49.625 48.125 48.5 47.875 47.5C45.75 40.875 39.375 36 32 36C24.5 36 18.125 40.875 16 47.5C15.75 48.5 16.25 49.625 17.375 50C18.375 50.25 19.5 49.75 19.875 48.625ZM22 22C19.75 22 18 23.875 18 26C18 28.25 19.75 30 22 30C24.25 30 26 28.25 26 26C26 23.875 24.25 22 22 22ZM42 30C44.25 30 46 28.25 46 26C46 23.875 44.25 22 42 22C39.75 22 38 23.875 38 26C38 28.25 39.75 30 42 30Z"
                fill="url(#paint0_linear_363_12374)"
                fillOpacity={0.4}
            />
        </g>
        <defs>
            <filter
                id="filter0_i_363_12374"
                x={0}
                y={0}
                width={64}
                height={68}
                filterUnits="userSpaceOnUse"
                colorInterpolationFilters="sRGB"
            >
                <feFlood floodOpacity={0} result="BackgroundImageFix" />
                <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
                <feColorMatrix
                    in="SourceAlpha"
                    type="matrix"
                    values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    result="hardAlpha"
                />
                <feOffset dy={4} />
                <feGaussianBlur stdDeviation={2} />
                <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
                <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0.2 0 0 0 0 0.4 0 0 0 0.1 0" />
                <feBlend mode="normal" in2="shape" result="effect1_innerShadow_363_12374" />
            </filter>
            <linearGradient
                id="paint0_linear_363_12374"
                x1={64.16}
                y1={7.63142e-7}
                x2={0.160202}
                y2={64.1598}
                gradientUnits="userSpaceOnUse"
            >
                <stop stopColor="#96AAB7" />
                <stop offset={1} stopColor="#728896" />
            </linearGradient>
        </defs>
    </svg>
);
