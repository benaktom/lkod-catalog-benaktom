import Link from "next/link";
import React, { DetailedHTMLProps, forwardRef, HTMLAttributes } from "react";

import { ChevronIcon } from "@/components/icons/ChevronIcon";

import styles from "./ColorLink.module.scss";

interface ColorLinkProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    linkText: string;
    linkUrl?: string;
    onClick?: () => void;
    blank?: boolean;
    center?: boolean;
    start?: boolean;
    end?: boolean;
    direction: "up" | "down" | "left" | "right";
}

const ColorLink = forwardRef<HTMLDivElement, ColorLinkProps>(
    ({ linkText, linkUrl, onClick, blank, start, center, end, direction, ...restProps }: ColorLinkProps, ref) => {
        return (
            <span
                className={`${styles.container} ${center ? styles["container_position_center"] : ""} ${
                    start ? styles["container_position_start"] : ""
                } ${end ? styles["container_position_end"] : ""}`}
                {...restProps}
                ref={ref}
            >
                {onClick && (
                    <button className={styles["color-link"]} onClick={onClick} type="button">
                        {linkText}
                        <ChevronIcon color={`secondary`} direction={direction} />
                    </button>
                )}
                {!onClick && linkUrl && (
                    <span className={styles["color-link"]}>
                        <Link href={linkUrl}>
                            <a target={blank ? "_blank" : "_self"}>
                                {linkText}
                                <ChevronIcon color={`secondary`} direction={direction} />
                            </a>
                        </Link>
                    </span>
                )}
                {!onClick && !linkUrl && (
                    <span className={styles["color-link"]}>
                        {linkText}
                        <ChevronIcon color={`secondary`} direction={direction} />
                    </span>
                )}
            </span>
        );
    }
);

export default ColorLink;
