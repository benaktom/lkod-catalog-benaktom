import React, { FC } from "react";

import styles from "./Sorter.module.scss";

type Props = {
    sort?: () => void;
    label: string;
};

const Sorter: FC<Props> = ({ label, sort }) => {
    return (
        <div className={styles.sorter}>
            <button onClick={sort}>{`<> `}</button>
            <span>{label}</span>
        </div>
    );
};

export default Sorter;
