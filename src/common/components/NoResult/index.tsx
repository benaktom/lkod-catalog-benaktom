import React, { DetailedHTMLProps, FC, HTMLAttributes } from "react";
import { useTranslation } from "react-i18next";

import { CardInfo } from "@/components/CardInfo";
import { Heading } from "@/components/Heading";
import { NoResultIcon } from "@/components/icons/NoResultIcon";
import { Text } from "@/components/Text";

import styles from "./NoResult.module.scss";

type Props = DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>;

export const NoResult: FC<Props> = () => {
    const { t } = useTranslation();

    return (
        <CardInfo className={styles["no-result"]}>
            <NoResultIcon />
            <Heading tag={`h2`}>{t("noresultHeading")}</Heading>
            <Text size={`lg`} className={styles.text}>
                {t("noresultText")}
            </Text>
        </CardInfo>
    );
};
