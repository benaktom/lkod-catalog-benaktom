import React, { memo } from "react";
import { useTranslation } from "react-i18next";

import logo_golemio from "@/images/logos/logo_golemi_gray.svg";
import logo_oict from "@/images/logos/logo_oict_gray.svg";
import logo_portalhmlp from "@/images/logos/logo_portalhlmp_gray.svg";

import styles from "./Logos.module.scss";
import SocialIcon from "./Socials/SocialIcon";

const logos = [
    { name: "Logo OICT", url: logo_oict, link: "https://operatorict.cz/" },
    { name: "Logo Golemio", url: logo_golemio, link: "https://golemio.cz/" },
    { name: "Logo Portál Hl.m.P.", url: logo_portalhmlp, link: "https://www.praha.eu/jnp/" },
];

const Logos = () => {
    const { t } = useTranslation();

    return (
        <div className={styles["logos-container"]}>
            <p>{t("footer.logosHeading")}</p>
            <div className={styles["logos"]}>
                {logos.map((icon, i) => {
                    return <SocialIcon label={icon.name} url={icon.url} link={icon.link} key={i} />;
                })}
            </div>
        </div>
    );
};

export default memo(Logos);
