import React, { FC, memo } from "react";
import { useTranslation } from "react-i18next";

import facebook from "@/images/facebook.svg";
// import instagram from "@/images/instagram.svg";
import twitter from "@/images/twitter.svg";

import SocialIcon from "./SocialIcon";
import styles from "./Socials.module.scss";

const socialIcons = [
    { name: "Facebook", url: facebook, link: "https://www.facebook.com/benaktom" },
    // { name: "Instagram", url: instagram, link: "https://www.instagram.com/cityofprague" },
    { name: "Twitter", url: twitter, link: "https://twitter.com/benaktom" },
];

const Socials: FC = () => {
    const { t } = useTranslation();

    return (
        <div className={styles.socials}>
            <p>{t("footer.socialsHeading")}</p>
            <div className={styles["social-icons"]}>
                {socialIcons.map((icon, i) => {
                    return <SocialIcon label={icon.name} url={icon.url} link={icon.link} key={i} />;
                })}
            </div>
        </div>
    );
};

export default memo(Socials);
