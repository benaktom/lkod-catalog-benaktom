import Image from "next/image";
import Link from "next/link";
import React, { FC, memo } from "react";

type SocialIcon = {
    link: string;
    url: string;
    label: string;
};

const SocialIcon: FC<SocialIcon> = ({ link, url, label }) => {
    return (
        <Link href={link}>
            <a target="_blank" rel="norefferer">
                <Image src={url} alt={label} />
            </a>
        </Link>
    );
};

export default memo(SocialIcon);
