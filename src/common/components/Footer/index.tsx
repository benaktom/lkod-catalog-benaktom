import Link from "next/link";
import React, { memo } from "react";
import { useTranslation } from "react-i18next";

import { Logo } from "@/components/configurable/Logo";
import Socials from "@/components/Footer/Socials";
import HorizontalLine from "@/components/HorizontalLine";
import { LoginIcon } from "@/components/icons/LoginIcon";
import useWindowSize from "@/hooks/useWindowSize";
import { ISize } from "@/hooks/useWindowSize";
import { config } from "@/root/config";

import styles from "./Footer.module.scss";
import Logos from "./Logos";

const Conditions = () => {
    const { t } = useTranslation();
    return (
        <div className={styles.conditions}>
            <Link href="/cookies">{"Cookies"}</Link>
            <Link href="/accessibility">{t("footer.accessibilityLink")}</Link>
            {config.SHOW_ADMINLOGIN_LINK !== "false" && (
                <Link href={`${config.ADMIN_URL}/login`}>
                    <a className={styles["red-link"]}>
                        {t("footer.login")}
                        <LoginIcon color={`secondary`} />
                    </a>
                </Link>
            )}
        </div>
    );
};

const Footer = () => {
    const size: ISize = useWindowSize();

    return (
        <footer className={styles.footer}>
            <div className={styles["container"]}>
                <Logos />
                <Socials />
                <HorizontalLine />
            </div>
            <div className={styles["container"]}>
                <Conditions />
                {size.width && size.width > 991 ? "" : <HorizontalLine />}
                <div className={styles["logo-container"]}>
                    <Logo footer src={""} />
                </div>
            </div>
        </footer>
    );
};

export default memo(Footer);
