import Link from "next/link";
import React from "react";
import { useTranslation } from "react-i18next";

import styles from "./LogoText.module.scss";

type LogoTextProps = {
    isFooter?: boolean;
};

export const LogoText = ({ isFooter }: LogoTextProps) => {
    const { t } = useTranslation();

    return (
        <div className={`${styles.text} ${isFooter ? styles["text-footer"] : undefined}`}>
            <Link href="/">
                <a className={styles.link}>{t("configurable.logoText")}</a>
            </Link>
        </div>
    );
};
