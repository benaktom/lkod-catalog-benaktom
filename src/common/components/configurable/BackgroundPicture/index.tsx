import Image from "next/image";
import React, { FC } from "react";

import mainBackgroundImage from "@/images/mainBackgroundImage.webp";
import projectPageBackgroudImage from "@/images/projectPageBackgroudImage.webp";

import styles from "./BackgroundPicture.module.scss";

type Props = {
    isHomepage?: boolean;
};

export const BackgroundPicture: FC<Props> = ({ isHomepage }) => {
    const imagePath = isHomepage ? mainBackgroundImage : projectPageBackgroudImage;

    return <Image src={imagePath} alt="obrázek na pozadí" layout="fill" priority className={styles.background} />;
};
