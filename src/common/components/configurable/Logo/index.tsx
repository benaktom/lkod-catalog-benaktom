import Image, { ImageProps } from "next/image";
import Link from "next/link";
import React, { forwardRef } from "react";
import { useTranslation } from "react-i18next";

import logo from "@/logos/logoOwner.svg";

import styles from "./Logo.module.scss";

type LogoProps = ImageProps & {
    footer?: boolean;
};

export const Logo = forwardRef<HTMLDivElement, LogoProps>(({ footer }, ref) => {
    const { t } = useTranslation();

    return (
        <Link href="/">
            <a className={`${styles["frame"]}`}>
                <div className={`${styles["logo-frame"]} ${footer ? styles["logo-frame_footer"] : ""}`} ref={ref}>
                    <Image src={logo} alt={`${t("configurable.logoOwner")}`} width={96} height={96} />
                </div>
                <div className={`${styles.text} ${footer ? styles["text-footer"] : undefined}`}>
                    <p className={styles.link}>{t("configurable.logoText")}</p>
                </div>
            </a>
        </Link>
    );
});
