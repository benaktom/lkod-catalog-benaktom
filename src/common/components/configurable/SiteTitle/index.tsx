import React, { FC } from "react";
import { useTranslation } from "react-i18next";

import { Heading } from "@/components/Heading";

import styles from "./SiteTitle.module.scss";

export const SiteTitle: FC = () => {
    const { t } = useTranslation();

    return (
        <Heading tag={`h1`} className={`${styles["site-title"]} ${styles["site-title__first-line"]}`}>
            {t("configurable.siteTitle")}
        </Heading>
    );
};
