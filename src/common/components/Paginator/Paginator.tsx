import { useRouter } from "next/router";
import { Dispatch, FC, memo, SetStateAction, useEffect } from "react";

import { ChevronIcon } from "@/components/icons/ChevronIcon";

import styles from "./Paginator.module.scss";

type PaginatorProps = {
    totalCount: number;
    itemsPerPage: number;
    currentPage: number;
    setCurrentPage: Dispatch<SetStateAction<number>>;
    nItemsWithin?: number;
};

const Paginator: FC<PaginatorProps> = ({ totalCount, itemsPerPage, currentPage, setCurrentPage, nItemsWithin = 1 }) => {
    const router = useRouter();
    const pageCount = Math.ceil(totalCount / itemsPerPage) || 1;
    const isPaginationShown = pageCount > 1;
    const isCurrentPageFirst = currentPage === 1;
    const isCurrentPageLast = currentPage === pageCount;

    const changePage = (pageNumber: number) => {
        if (currentPage === pageNumber) return;
        setCurrentPage(pageNumber);
    };

    const onPageNumberClick = (pageNumber: number) => {
        if (pageNumber !== currentPage) {
            changePage(pageNumber);
            router.push(
                {
                    query: {
                        ...router.query,
                        page: `${pageNumber === currentPage ? "" : pageNumber}`,
                    },
                },
                undefined,
                { shallow: true }
            );
        }
        return;
    };

    const onPreviousPageClick = () => {
        if (!isCurrentPageFirst) {
            changePage(currentPage - 1);
            router.push({ query: { ...router.query, page: `${isCurrentPageFirst ? "" : currentPage - 1}` } }, undefined, {
                shallow: true,
            });
        }
    };

    const onNextPageClick = () => {
        if (!isCurrentPageLast) {
            changePage(currentPage + 1);
            router.push(
                { query: { ...router.query, page: `${isCurrentPageLast ? pageCount + 1 : currentPage + 1}` } },
                undefined,
                { shallow: true }
            );
        }
    };

    const setLastPageAsCurrent = () => {
        if (currentPage > pageCount) {
            setCurrentPage(pageCount);
            router.push({ query: { ...router.query, page: `${pageCount}` } }, undefined, { shallow: true });
        }
    };

    let isPageNumberOutOfRange: boolean;

    const pageNumbers = [...new Array(pageCount)].map((_, index) => {
        const pageNumber = index + 1;

        const isPageNumberFirst = pageNumber === 1;
        const isPageNumberLast = pageNumber === pageCount;
        const isCurrentPageWithinNPageNumbers = Math.abs(pageNumber - currentPage) <= nItemsWithin;

        if (isPageNumberFirst || isPageNumberLast || isCurrentPageWithinNPageNumbers) {
            isPageNumberOutOfRange = false;
            return (
                <div
                    key={pageNumber}
                    className={`${styles["paginator__item"]} ${
                        pageNumber === currentPage ? styles["paginator__item_active"] : ""
                    }`}
                    onClick={() => onPageNumberClick(pageNumber)}
                >
                    {pageNumber}
                </div>
            );
        }

        if (!isPageNumberOutOfRange) {
            isPageNumberOutOfRange = true;
            return <div key={`elipsis${index}`} className={`${styles.elipsis} ${styles.elipsis_disabled}`} />;
        }

        return null;
    });

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(setLastPageAsCurrent, [currentPage, pageCount, router, setCurrentPage]);

    return (
        <>
            {isPaginationShown && (
                <div className={styles.paginator}>
                    <button
                        key={`left`}
                        aria-label={"Předchozí stránka"}
                        onClick={onPreviousPageClick}
                        className={styles.arrow}
                        type="button"
                    >
                        <ChevronIcon color={`primary`} direction={`left`} />
                    </button>
                    {pageNumbers}
                    <button
                        key={`right`}
                        aria-label={"Následující stránka"}
                        onClick={onNextPageClick}
                        className={styles.arrow}
                        type="button"
                    >
                        <ChevronIcon color={`primary`} direction={`right`} />
                    </button>
                </div>
            )}
        </>
    );
};

export default memo(Paginator);
