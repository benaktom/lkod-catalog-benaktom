import Image from "next/image";
import { useEffect, useState } from "react";

import LKOD_Technically from "@/images/lkod-technically.webp";
import LKOD_Technically_SmallScreen from "@/images/lkod-technicaly-media.webp";

const ImageChange = () => {
    const [isSmallScreen, setIsSmallScreen] = useState<boolean>(typeof window !== "undefined" && window.innerWidth <= 576);

    useEffect(() => {
        const handleResize = () => {
            setIsSmallScreen(window.innerWidth <= 576);
        };
        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
    }, []);

    return (
        <Image
            src={isSmallScreen ? LKOD_Technically_SmallScreen : LKOD_Technically}
            alt="LKOD_technically"
            layout="responsive"
            objectFit="contain"
        />
    );
};

export default ImageChange;
