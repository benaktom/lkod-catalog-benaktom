import Image from "next/image";
import React, { DetailedHTMLProps, FC, HTMLAttributes } from "react";

import { Heading } from "@/components/Heading";

import styles from "./OrganizationIconFrame.module.scss";
interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    src: string | null;
    alt: string;
    width?: string;
    height?: string;
    name: string;
    description?: string | null;
}

const OrgIconFrame: FC<Props> = ({ src, alt, width = "152.5rem", height = "152.5rem", name, description }) => {
    return (
        <div className={styles["frame-wrapper"]}>
            <div className={styles["icon-box"]}>
                <p>{"Organizace"}</p>
                {src && <Image src={src} width={width} height={height} alt={alt} />}
                <Heading tag={`h2`} type={`h5`}>
                    {name}
                </Heading>
                <p>{description}</p>
            </div>
        </div>
    );
};

export default OrgIconFrame;
