import Head from "next/head";
import { useRouter } from "next/router";

import { config } from "@/root/config";
import Meta from "@/root/metatags.json";
import { Dataset } from "@/schema/dataset";

export interface IMetaData extends Dataset {
    title: string;
    description: string | null;
    keywords: string[];
    ogTitle: string;
    ogDescription: string;
    ogImage: string;
    twitterImage: string;
}
interface LayoutProps {
    metaData: IMetaData;
}

const HeadContainer = (props: LayoutProps) => {
    const router = useRouter();

    const isDatasetDetail = router.pathname === "/datasets/[slug]";

    const { metaData } = props;
    const pageTitle = metaData?.title;
    const description = metaData?.description || Meta.appDescription;
    const keywords = metaData?.keywords || Meta.keywords;
    const ogTitle = isDatasetDetail ? metaData.title : metaData?.ogTitle || Meta.appTitle;
    const ogDescription = isDatasetDetail ? metaData.description : metaData?.ogDescription || Meta.appDescription;
    const ogImage = isDatasetDetail ? Meta.datasetsPage.ogImage : metaData?.ogImage || Meta.ogDefault.ogImage;
    const twitterImage = isDatasetDetail ? Meta.datasetsPage.twitterImage : metaData?.twitterImage || Meta.ogDefault.twitterImage;

    return (
        <Head>
            <title>{`${Meta.appTitle} ${pageTitle ? `| ${pageTitle}` : ``}`}</title>
            <meta name="title" content={`${Meta.appTitle} ${pageTitle ? `| ${pageTitle}` : ``}`} />
            <meta name="description" content={description} />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta name="keywords" content={keywords.toString()} />
            <meta property="og:title" content={ogTitle} />
            <meta property="og:description" content={ogDescription ?? ""} />
            <meta property="og:image" content={`${config.PUBLIC_URL}${ogImage}`} />
            <meta property="og:image:alt" content={`náhled stránky ${Meta.appTitle} ${pageTitle ? `| ${pageTitle}` : ``}`} />
            <meta property="og:image:width" content="1200" />
            <meta property="og:image:height" content="630" />
            <meta property="twitter:image" content={`${config.PUBLIC_URL}${twitterImage}`} />
            <meta property="twitter:image:alt" content={`náhled stránky ${Meta.appTitle} ${pageTitle ? `| ${pageTitle}` : ``}`} />
            <link rel="icon" href="/favicon.ico" />
            <link rel="canonical" href={`${config.PUBLIC_URL}${router.asPath}`} />
            <meta name="google-site-verification" content={config.GOOGLE_SITE_VERIFICATION} />
        </Head>
    );
};
export default HeadContainer;
