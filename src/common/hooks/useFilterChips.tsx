/* eslint-disable react-hooks/exhaustive-deps */
import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";

import { IListFormat, IListKeyword, IListPublisher, IListTheme } from "@/root/schema";
import { useDataCtx } from "@/root/store/dataContext";
import { CatalogService } from "@/services/catalog-service";
import { IFilter } from "@/types/FilterInterface";

const service = new CatalogService();

export const useFilterChips = () => {
    const router = useRouter();

    const { publisherIri, themeIris, keywords, formatIris } = router.query;

    const [publishers, setPublishers] = useState<IListPublisher[]>();
    const [topics, setTopics] = useState<IListTheme[]>();
    const [keys, setKeys] = useState<IListKeyword[]>();
    const [formats, setFormats] = useState<IListFormat[]>();

    const { ctxFilter } = useDataCtx();

    const [isLoading, setisLoading] = useState(false);

    const getPublisherChip = useCallback(async (filter?: IFilter, queryPublisher?: string) => {
        setisLoading(true);
        try {
            const data = await service.getPublishers({ filter });
            if (!queryPublisher) {
                setPublishers([]);
            } else {
                setPublishers([data[data.findIndex((element) => element.iri === queryPublisher)]]);
            }
        } catch (error) {
            console.log(error);
        }
        setisLoading(false);
    }, []);

    const getTopicChips = useCallback(async (filter?: IFilter, queryTopic?: string | string[]) => {
        setisLoading(true);
        try {
            const data = await service.getThemes({ filter });
            if (!queryTopic) {
                setTopics([]);
            } else if (typeof queryTopic === "string") {
                setTopics([data[data.findIndex((element) => element.iri === queryTopic)]]);
            } else {
                setTopics(queryTopic.map((el) => data.filter((e) => e.iri === el)).flat(1));
            }
        } catch (error) {
            console.log(error);
        }
        setisLoading(false);
    }, []);

    const getKeys = useCallback(async (filter?: IFilter, queryKey?: string | string[]) => {
        setisLoading(true);
        try {
            const data = await service.getKeywords({ filter });
            if (!queryKey) {
                setKeys([]);
            } else if (typeof queryKey === "string") {
                setKeys([data[data.findIndex((element) => element.label === queryKey)]]);
            } else {
                setKeys(queryKey.map((el) => data.filter((e) => e.label === el)).flat(1));
            }
        } catch (error) {
            console.log(error);
        }
        setisLoading(false);
    }, []);

    const getFormatChips = useCallback(async (filter?: IFilter, queryFormat?: string | string[]) => {
        setisLoading(true);
        try {
            const data = await service.getFormats({ filter });
            if (!queryFormat) {
                setFormats([]);
            } else if (typeof queryFormat === "string") {
                setFormats([data[data.findIndex((element) => element.iri === queryFormat)]]);
            } else {
                setFormats(queryFormat.map((el) => data.filter((e) => e.iri === el)).flat(1));
            }
        } catch (error) {
            console.log(error);
        }
        setisLoading(false);
    }, []);

    useEffect(() => {
        if (router.isReady) {
            getPublisherChip(ctxFilter, publisherIri as string);
            getTopicChips(ctxFilter, themeIris);
            getKeys(ctxFilter, keywords);
            getFormatChips(ctxFilter, formatIris);
        }
    }, [ctxFilter, formatIris, publisherIri, router.isReady, keywords, themeIris]);

    return { topics, publishers, keys, formats, isLoading };
};
