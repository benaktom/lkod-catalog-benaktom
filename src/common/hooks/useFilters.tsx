import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";

import { IListFormat, IListKeyword, IListPublisher, IListTheme } from "@/root/schema";
import { useDataCtx } from "@/root/store/dataContext";
import { CatalogService } from "@/services/catalog-service";
import { IFilter } from "@/types/FilterInterface";

const service = new CatalogService();

export const useFilters = () => {
    const router = useRouter();

    const [topics, setTopics] = useState<IListTheme[]>();
    const [publishers, setPublishers] = useState<IListPublisher[]>();
    const [keywords, setKeywords] = useState<IListKeyword[]>();
    const [formats, setFormats] = useState<IListFormat[]>();

    const { ctxFilter } = useDataCtx();

    const [isLoading, setisLoading] = useState(false);

    const getTopics = useCallback(async (filter?: IFilter) => {
        setisLoading(true);
        try {
            const data = await service.getThemes({ filter });
            setTopics(data);
        } catch (error) {
            console.log(error);
        }
        setisLoading(false);
    }, []);

    const getPublishers = useCallback(async (filter?: IFilter) => {
        setisLoading(true);
        try {
            const data = await service.getPublishers({ filter });
            setPublishers(data);
        } catch (error) {
            console.log(error);
        }
        setisLoading(false);
    }, []);

    const getKeywords = useCallback(async (filter?: IFilter) => {
        setisLoading(true);
        try {
            const data = await service.getKeywords({ filter });
            setKeywords(data);
        } catch (error) {
            console.log(error);
        }
        setisLoading(false);
    }, []);

    const getFormats = useCallback(async (filter?: IFilter) => {
        setisLoading(true);
        try {
            const data = await service.getFormats({ filter });
            setFormats(data);
        } catch (error) {
            console.log(error);
        }
        setisLoading(false);
    }, []);

    useEffect(() => {
        if (router.isReady) {
            getPublishers(ctxFilter);
            getTopics(ctxFilter);
            getKeywords(ctxFilter);
            getFormats(ctxFilter);
        }
    }, [getTopics, getPublishers, getKeywords, getFormats, ctxFilter, router.isReady]);

    return { topics, publishers, keywords, formats, isLoading };
};
