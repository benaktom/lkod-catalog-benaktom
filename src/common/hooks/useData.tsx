/* eslint-disable react-hooks/exhaustive-deps */
import { useCallback, useEffect, useState } from "react";

import { DatasetsItem } from "@/root/schema";
import { CatalogService } from "@/services/catalog-service";
import { IFilter } from "@/types/FilterInterface";

const service = new CatalogService();

export const useData = ({
    count,
    perPage,
    currentPage,
    filter,
    searchString,
}: {
    count: number;
    perPage: number;
    currentPage: number;
    filter: IFilter;
    searchString: string;
}) => {
    const [data, setData] = useState<DatasetsItem[]>();
    const [resultCount, setResultCount] = useState(count);

    const [isLoading, setIsLoading] = useState(false);

    const getData = useCallback(
        async (filter: IFilter, searchString: string) => {
            setIsLoading(true);
            setData([]);
            try {
                const { datasets, count } = await service.findDatasets({
                    limit: perPage,
                    offset: (currentPage - 1) * perPage,
                    filter,
                    searchString,
                });
                setData(datasets);
                setResultCount(count);
            } catch (error: unknown) {
                console.log(error);
            }
            setIsLoading(false);
        },
        [currentPage, perPage]
    );

    useEffect(() => {
        getData(filter, searchString);
    }, [filter, searchString, currentPage, perPage]);

    return { data, resultCount, isLoading };
};
