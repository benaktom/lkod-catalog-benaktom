/* eslint-disable react-hooks/exhaustive-deps */
import { useCallback, useEffect, useState } from "react";

import { IPublisherData } from "@/root/schema";
import { CatalogService } from "@/services/catalog-service";

const service = new CatalogService();

export const useOrganizations = () => {
    const [publishers, setPublishers] = useState<IPublisherData[]>();

    const [isLoading, setisLoading] = useState(false);

    const getPublishers = useCallback(async () => {
        setisLoading(true);
        try {
            const data = await service.getAllPublishersData();
            setPublishers(data);
        } catch (error) {
            console.log(error);
        }
        setisLoading(false);
    }, []);

    useEffect(() => {
        getPublishers();
    }, []);

    return { publishers, isLoading };
};
