export interface IFilter {
    themeIris: string[];
    publisherIri: string;
    keywords: string[];
    formatIris: string[];
}

export type genericFilter = {
    [key: string]: string[] | string;
};

export interface INamedFilter {
    themeIris: { label: string; iri: string }[] | [];
    publisherIri: { label: string; iri: string }[] | [];
    keywords: { label: string; iri: string }[] | [];
    formatIris: { label: string; iri: string }[] | [];
}

export type genericNamedFilter = {
    [key: string]: { label: string; iri: string }[] | [];
};
