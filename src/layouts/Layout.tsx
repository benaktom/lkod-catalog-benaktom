import { useRouter } from "next/router";
import { FC, ReactNode } from "react";
import React from "react";

import Footer from "@/components/Footer";
import HeadContainer, { IMetaData } from "@/components/HeadContainer";
import Header from "@/components/Header";
import { useOrganizations } from "@/hooks/useOrganizations";

import styles from "./Layout.module.scss";
interface LayoutProps {
    title?: string;
    children: ReactNode;
    metaData: IMetaData;
}

const Layout: FC<LayoutProps> = ({ children, metaData }) => {
    const router = useRouter();

    const { publishers, isLoading } = useOrganizations();

    const onCookiesPage = router.pathname === "/cookies";

    const shrinkLayout = publishers && publishers.length === 0 && !isLoading;

    return (
        <>
            <HeadContainer metaData={metaData} />
            <div
                className={`${styles["main-layout"]} ${onCookiesPage ? styles["main-layout__cookie-page"] : ""} ${
                    shrinkLayout ? styles["main-layout__shrinked"] : ""
                }`}
            >
                <a className={styles["skip-link"]} href="#main">
                    Přeskočit na hlavní obsah
                </a>
                <Header />
                {children}
                <Footer />
            </div>
        </>
    );
};

export default Layout;
