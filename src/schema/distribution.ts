export interface RawDistribution {
    title?: string;
    format: string;
    formatIri: string;
    mediaType?: string;
    downloadUrl: string;
    accessUrl: string;
    compressFormat?: string;
    packageFormat?: string;
    conformsTo?: string;
}

export interface Distribution {
    iri: string;
    title: string | null;
    format: {
        title: string | null;
        iri: string | null;
    };
    mediaType: string | null;
    downloadUrl: string;
    accessUrl: string;
    compressFormat: string | null;
    packageFormat: string | null;
    conformsTo: string | null;
}

export interface RawDistributionService {
    title?: string;
    endpointURL?: string;
    endpointDescription?: string;
    conformsTo?: string;
}

export interface DistributionService {
    iri: string;
    title: string | null;
    endpointURL: string | null;
    endpointDescription: string | null;
    conformsTo: string | null;
}

export interface RawDistributionLicense {
    o1?: string;
    o2?: string;
    o3?: string;
    o4?: string;
}

export interface DistributionLicence {
    "autorské-dílo": string | null;
    "databáze-jako-autorské-dílo": string | null;
    "databáze-chráněná-zvláštními-právy": string | null;
    "osobní-údaje": string | null;
}
