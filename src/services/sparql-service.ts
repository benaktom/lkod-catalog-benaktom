/* eslint-disable @typescript-eslint/no-explicit-any */
import axios from "axios";

import { config } from "../config";

export interface SparqlResult<T extends { [key: string]: any }> {
    head: { link: string[]; vars: string[] };

    results: {
        distinct: boolean;
        ordered: boolean;
        bindings: {
            [K in keyof T]: {
                type: string;
                value: T[K];
                datatype?: string;
                "xml:lang"?: string;
            };
        }[];
    };
}

export interface DocumentFields {
    field: string;
    value: any;
}

export class SparqlService {
    async query<T>(query: string): Promise<T[]> {
        const response = await this.rawQuery<any>(query);

        return response.data.results.bindings.map((doc: any) => {
            return Object.entries(doc).reduce((acc, [key, value]: [key: any, value: any]) => {
                acc[key] = value.value;
                return acc;
            }, {} as any);
        });
    }

    rawQuery<T>(query: string) {
        return axios.get<T>(config.ENDPOINT, {
            params: { query },
            headers: { Accept: "application/json" },
        });
    }
}

const sparql = new SparqlService();
export { sparql };
