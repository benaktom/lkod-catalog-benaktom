/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-empty-function */
import {
    Dataset,
    DatasetsItem,
    Distribution,
    DistributionLicence,
    DistributionService,
    RawDataset,
    RawDatasetsItem,
    RawDistribution,
    RawDistributionLicense,
    RawDistributionService,
} from "../schema";
import {
    IListFormat,
    IListKeyword,
    IListPublisher,
    IListTheme,
    IPublisherData,
    IRawPublisherData,
    IRawThemeData,
    IThemeData,
} from "../schema/common";
import { sparql } from "./sparql-service";

export interface DatasetQueryOptions {
    limit?: number;
    offset?: number;
    filter?: {
        themeIris?: string[];
        publisherIri?: string;
        keywords?: string[];
        formatIris?: string[];
    };
    searchString?: string;
}

enum Prefix {
    dct = "http://purl.org/dc/terms/",
    dcat = "http://www.w3.org/ns/dcat#",
    skos = "http://www.w3.org/2004/02/skos/core#",
    foaf = "http://xmlns.com/foaf/0.1/",
    iana = "http://www.iana.org/assignments/media-types/",
    pu = "https://data.gov.cz/slovník/podmínky-užití/",
    vcard = "http://www.w3.org/2006/vcard/ns#",
    gr = "http://purl.org/goodrelations/v1#",
    owl = "http://www.w3.org/2002/07/owl#",
    schema = "http://schema.org/",
}

export class CatalogService {
    lang = "cs";

    constructor() {}

    async findDatasets(options: DatasetQueryOptions): Promise<{ count: number; datasets: DatasetsItem[] }> {
        const where = `
      WHERE {
        ?iri a dcat:Dataset ; dct:title ?title .
        OPTIONAL { ?iri dct:description ?description . FILTER (LANG(?description) = '${this.lang}') . }
        OPTIONAL {
          ?iri dct:publisher ?publisherIri . FILTER (!isBlank(?publisherIri)) .
          ?publisherIri foaf:name ?publisher . FILTER (LANG(?publisher) = '${this.lang}') .
        }
        OPTIONAL {
          ?iri dcat:theme ?themeIri . FILTER (!isBlank(?themeIri)) .
          ?themeIri skos:prefLabel ?theme . FILTER (LANG(?theme) = '${this.lang}') .
        }
        OPTIONAL {
          ?iri dcat:distribution ?distributionIri . FILTER (!isBlank(?distributionIri)) .
          ?distributionIri dct:format ?formatIri . FILTER (!isBlank(?formatIri)) .
          ?formatIri skos:prefLabel ?format . FILTER (LANG(?format) = 'en') .
        }
        FILTER (LANG(?title) = '${this.lang}') .
        ${this.createFilter(options, "?iri")}
        ${options.searchString ? this.createSearch(options.searchString) : ``}
      }
    `;

        const datasetsQuery = `${this.createPrefixes(["dcat", "dct", "foaf", "skos"])}
      SELECT
        ?iri
        ?title
        ?description
        ?publisher
        (group_concat(DISTINCT ?theme; separator="|") as ?rawThemes)
        (group_concat(DISTINCT ?format; separator="|") as ?rawFormats)
      ${where}
        GROUP BY ?iri ?title ?description ?publisher
        ORDER BY <http://jena.apache.org/ARQ/function#collation>('${this.lang}', ?title)
        LIMIT ${options.limit ?? 25}
        OFFSET ${options.offset ?? 0}
    `;

        const countQuery = `${this.createPrefixes(["dcat", "dct", "foaf", "skos"])}
      SELECT
        (COUNT(DISTINCT ?iri) AS ?count)
      ${where}
    `;

        const datasets = await sparql.query<RawDatasetsItem>(datasetsQuery);
        const count = (await sparql.query<{ count: number }>(countQuery))[0].count;

        return {
            count,
            datasets: await this.mapDatasets(datasets),
        };
    }

    async getDataset(iri: string): Promise<Dataset | null> {
        const datasetQuery = `${this.createPrefixes(["dct", "foaf", "skos", "dcat", "vcard", "gr", "schema"])}
      SELECT ?title ?description ?publisher ?publisherIri ?publisherLogo ?publisherDesc ?documentation ?specification ?accrualPeriodicity
        ?temporalFrom ?temporalTo ?temporalResolution ?spatialResolutionInMeters ?contactPointName ?contactPointEmail
      WHERE {
        <${iri}> dct:title ?title .
        FILTER(LANG(?title) = '${this.lang}') .
        OPTIONAL { <${iri}> dct:description ?description . FILTER(LANG(?description) = '${this.lang}') }
        OPTIONAL { <${iri}> foaf:page ?documentation . }
        OPTIONAL { <${iri}> dct:conformsTo ?specification . }
        OPTIONAL { <${iri}> dct:publisher ?publisherIri . }
        OPTIONAL {
          ?publisherIri foaf:name ?publisher .
          FILTER(LANG(?publisher) = '${this.lang}') .
        }
        OPTIONAL { ?publisherIri schema:logo ?publisherLogo . }
        OPTIONAL {
          ?publisherIri schema:description ?publisherDesc .
          FILTER(LANG(?publisherDesc) = '${this.lang}') .
        }
        OPTIONAL {
          <${iri}> dct:accrualPeriodicity ?accrualPeriodicityIri .
          ?accrualPeriodicityIri skos:prefLabel ?accrualPeriodicity .
          FILTER(LANG(?accrualPeriodicity) = '${this.lang}')
        }
        OPTIONAL { <${iri}> dct:temporal [ dcat:startDate ?temporalFrom ; dcat:endDate ?temporalTo ] . }
        OPTIONAL { <${iri}> dcat:temporalResolution ?temporalResolution . }
        OPTIONAL { <${iri}> dcat:spatialResolutionInMeters ?spatialResolutionInMeters . }
        OPTIONAL {
          <${iri}> dcat:contactPoint [ vcard:fn ?contactPointName ; vcard:hasEmail ?contactPointEmail ]
          FILTER(LANG(?contactPointName) = '${this.lang}') .
        }
      }
      `;

        const rawDataset = (await sparql.query<RawDataset>(datasetQuery))[0];

        if (!rawDataset) {
            return null;
        }

        const keywordsQuery = `${this.createPrefixes(["dcat"])}
      SELECT ?keyword
      WHERE {
        <${iri}> dcat:keyword ?keyword
        FILTER ( LANG(?keyword) = '${this.lang}' )
      }`;
        const keywords = await sparql
            .query<{ keyword: string }>(keywordsQuery)
            .then((results) => results.map((result) => result.keyword));

        const themesQuery = `${this.createPrefixes(["skos", "dcat", "owl", "schema"])}
      SELECT ?iri ?title ?image
      WHERE {
        <${iri}> dcat:theme ?iri .
        ?iri skos:prefLabel ?title .
        OPTIONAL { ?iri2 owl:sameAs ?iri . ?iri2 schema:image ?image }
        FILTER ( strstarts(str(?iri), 'http://publications.europa.eu') )
        FILTER ( LANG(?title) = '${this.lang}' )
      }`;
        const themes = await sparql.query<{ iri: string; title: string; image: string }>(themesQuery);

        const eurovocThemesQuery = `${this.createPrefixes(["skos", "dcat"])}
      SELECT ?iri
      WHERE {
        <${iri}> dcat:theme ?iri .
        FILTER ( strstarts(str(?iri), 'http://eurovoc.europa.eu') )
      }`;
        const eurovocThemes = await sparql
            .query<{ iri: string }>(eurovocThemesQuery)
            .then((results) => results.map((result) => result.iri));

        const spatialQuery = `${this.createPrefixes(["dct"])} SELECT ?spatial WHERE { <${iri}> dct:spatial ?spatial . }`;
        const spatial = await sparql
            .query<{ spatial: string }>(spatialQuery)
            .then((results) => results.map((result) => result.spatial));

        const distributionsQuery = `${this.createPrefixes(["dcat", "dct"])}
      SELECT ?distributionIri ?accessService ?title
      WHERE {
        <${iri}> dcat:distribution ?distributionIri .
        OPTIONAL { ?distributionIri dcat:accessService ?accessService . }
        OPTIONAL { ?distributionIri dct:title ?title . FILTER ( LANG(?title) = '${this.lang}' ) }
        FILTER (!isBlank(?distributionIri)) .
      }`;
        const distributions = await sparql.query<{
            distributionIri: string;
            accessService?: string;
            title?: string;
        }>(distributionsQuery);
        const distributionFiles = [];
        const distributionsServices = [];
        for (const distribution of distributions) {
            if (distribution.accessService) {
                distributionsServices.push({
                    distributionIri: distribution.distributionIri,
                    accessServiceIri: distribution.accessService,
                    title:
                        distribution.title ||
                        distribution.distributionIri.split("/")[distribution.distributionIri.split("/").length - 1],
                });
            } else {
                distributionFiles.push({
                    distributionIri: distribution.distributionIri,
                    title:
                        distribution.title ||
                        distribution.distributionIri.split("/")[distribution.distributionIri.split("/").length - 1],
                });
            }
        }

        return {
            iri,
            title: rawDataset.title,
            description: rawDataset.description || null,
            publisher:
                rawDataset.publisherIri && rawDataset.publisher
                    ? {
                          iri: rawDataset.publisherIri,
                          title: rawDataset.publisher,
                          logo: rawDataset.publisherLogo || null,
                          description: rawDataset.publisherDesc || null,
                      }
                    : null,
            documentation: rawDataset.documentation || null,
            specification: rawDataset.specification || null,
            accrualPeriodicity: rawDataset.accrualPeriodicity || null,
            temporalFrom: rawDataset.temporalFrom || null,
            temporalTo: rawDataset.temporalTo || null,
            temporalResolution: rawDataset.temporalResolution || null,
            spatialResolutionInMeters: rawDataset.spatialResolutionInMeters || null,
            contactPoint:
                rawDataset.contactPointName && rawDataset.contactPointEmail
                    ? {
                          name: rawDataset.contactPointName,
                          email: rawDataset.contactPointEmail,
                      }
                    : null,
            keywords,
            themes,
            eurovocThemes,
            distributions: {
                files: distributionFiles,
                services: distributionsServices,
            },
            spatial,
        };
    }

    async getDistribution(iri: string): Promise<Distribution> {
        const query = `${this.createPrefixes(["dct", "dcat", "skos"])}
      SELECT ?title ?format ?formatIri ?mediaType ?downloadUrl ?accessUrl ?compressFormat ?packageFormat ?conformsTo ?accessService
      WHERE {
        OPTIONAL {
          <${iri}> dct:title ?title .
          FILTER(LANG(?title) = '${this.lang}') .
        }
        OPTIONAL {
          <${iri}> dct:format ?formatIri .
          ?formatIri skos:prefLabel ?format .
          FILTER(LANG(?format) = 'en') .
        }
        OPTIONAL { <${iri}> dcat:mediaType ?mediaType . }
        OPTIONAL { <${iri}> dcat:downloadURL ?downloadUrl . }
        OPTIONAL { <${iri}> dcat:accessURL ?accessUrl . }
        OPTIONAL { <${iri}> dcat:compressFormat ?compressFormat . }
        OPTIONAL { <${iri}> dcat:packageFormat ?packageFormat . }
        OPTIONAL { <${iri}> dct:conformsTo ?conformsTo . }
      }
      LIMIT 1
      `;
        const metadata = (await sparql.query<RawDistribution>(query))[0];

        if (Object.keys(metadata).length === 0) {
            throw new Error("Not found");
        }

        return {
            iri,
            title: metadata.title || null,
            format: {
                title: metadata.format || null,
                iri: metadata.formatIri || null,
            },
            mediaType: metadata.mediaType?.replace(Prefix.iana, "") || null,
            downloadUrl: metadata.downloadUrl,
            accessUrl: metadata.accessUrl,
            compressFormat: metadata.compressFormat?.replace(Prefix.iana, "") || null,
            packageFormat: metadata.packageFormat?.replace(Prefix.iana, "") || null,
            conformsTo: metadata.conformsTo || null,
        };
    }

    async getDistributionService(iri: string): Promise<DistributionService> {
        const query = `${this.createPrefixes(["dct", "dcat"])}
        SELECT ?title ?endpointURL ?endpointDescription ?conformsTo
        WHERE {
          OPTIONAL { <${iri}> dct:title ?title . FILTER(LANG(?title) = '${this.lang}') . }
          OPTIONAL { <${iri}> dcat:endpointURL ?endpointURL . }
          OPTIONAL { <${iri}> dcat:endpointDescription ?endpointDescription . }
          OPTIONAL { <${iri}> dct:conformsTo ?conformsTo . }
        }
        LIMIT 1`;

        const result = (await sparql.query<RawDistributionService>(query))[0];

        if (Object.keys(result).length === 0) {
            throw new Error("Not found");
        }

        return {
            iri,
            title: result.title || null,
            endpointURL: result.endpointURL || null,
            endpointDescription: result.endpointDescription || null,
            conformsTo: result.conformsTo || null,
        };
    }

    async getDistributionLicense(iri: string): Promise<DistributionLicence> {
        const query = `${this.createPrefixes(["pu"])}
      SELECT ?o1 ?o2 ?o3 ?o4
      WHERE {
        OPTIONAL { <${iri}> pu:specifikace [ pu:autorské-dílo ?o1 ] . }
        OPTIONAL { <${iri}> pu:specifikace [ pu:databáze-jako-autorské-dílo ?o2 ] . }
        OPTIONAL { <${iri}> pu:specifikace [ pu:databáze-chráněná-zvláštními-právy ?o3 ] . }
        OPTIONAL { <${iri}> pu:specifikace [ pu:osobní-údaje ?o4 ] . }
      }`;
        const result = (await sparql.query<RawDistributionLicense>(query))[0];

        if (Object.keys(result).length === 0) {
            throw new Error("Not found");
        }

        return {
            "autorské-dílo": result.o1 || null,
            "databáze-jako-autorské-dílo": result.o2 || null,
            "databáze-chráněná-zvláštními-právy": result.o3 || null,
            "osobní-údaje": result.o4 || null,
        };
    }

    async getPublishers(options: DatasetQueryOptions): Promise<IListPublisher[]> {
        const query = `${this.createPrefixes(["foaf", "dcat", "dct"])}
      SELECT ?iri (SAMPLE(?labels) AS ?label) (COUNT(DISTINCT ?datasetIri) as ?count)
      WHERE {
        ?datasetIri a dcat:Dataset ; dct:publisher ?iri .
        ?iri foaf:name ?labels .
        FILTER(LANG(?labels) = '${this.lang}')
        ${this.createFilter(options, "?datasetIri")}
      }
      GROUP BY ?iri
      ORDER BY DESC(?count)`;

        return sparql.query<IListPublisher>(query);
    }

    async getThemes(options: DatasetQueryOptions): Promise<IListTheme[]> {
        const query = `${this.createPrefixes()}
      SELECT ?iri (SAMPLE(?prefLabel) AS ?label) (COUNT(DISTINCT ?datasetIri) as ?count)
      WHERE {
        ?datasetIri a dcat:Dataset ; dcat:theme ?iri .
        ?iri skos:prefLabel ?prefLabel .
        FILTER(LANG(?prefLabel) = '${this.lang}')
        ${this.createFilter(options, "?datasetIri")}
      }
      GROUP BY ?iri
      ORDER BY DESC(?count)`;

        return sparql.query<IListTheme>(query);
    }

    async getKeywords(options: DatasetQueryOptions): Promise<IListKeyword[]> {
        const query = `${this.createPrefixes()}
      SELECT ?label (COUNT(DISTINCT ?datasetIri) as ?count)
      WHERE {
        ?datasetIri a dcat:Dataset ; dcat:keyword ?label .
        FILTER(LANG(?label) = '${this.lang}') .
        ${this.createFilter(options, "?datasetIri")}
      }
      GROUP BY ?label
      ORDER BY DESC(?count)`;
        return sparql.query<IListKeyword>(query);
    }

    async getFormats(options: DatasetQueryOptions): Promise<IListFormat[]> {
        const query = `${this.createPrefixes()}
      SELECT ?iri (SAMPLE(?prefLabel) AS ?label) (COUNT(DISTINCT ?datasetIri) as ?count)
      WHERE {
        ?datasetIri a dcat:Dataset ; dcat:distribution ?distributionIri .
        ?distributionIri dct:format ?iri .
        ?iri skos:prefLabel ?prefLabel .
        FILTER(LANG(?prefLabel) = 'en') .
        ${this.createFilter(options, "?datasetIri")}
      }
      GROUP BY ?iri
      ORDER BY DESC(?count)`;
        return sparql.query<IListFormat>(query);
    }

    async getAllPublishersData(): Promise<IPublisherData[]> {
        const query = `${this.createPrefixes(["foaf", "dcat", "dct", "schema"])}
      SELECT ?iri ?name ?logo ?description ?slug (COUNT(DISTINCT ?datasetIri) as ?count)
      WHERE {
          ?iri a schema:Organization .
          ?iri foaf:name ?name .
          OPTIONAL { ?iri schema:logo ?logo } .
          OPTIONAL { ?iri schema:description ?description }.
          OPTIONAL { ?iri schema:alternateName ?slug } .
          OPTIONAL { ?datasetIri a dcat:Dataset ; dct:publisher ?iri . }
          FILTER(LANG(?name) = '${this.lang}')
      }
      GROUP BY ?iri ?name ?logo ?description ?slug
      ORDER BY DESC(?count) ASC(?name)`;
        const results = await sparql.query<IRawPublisherData>(query);
        return results.map((result) => ({
            iri: result.iri,
            name: result.name,
            logo: result.logo || null,
            description: result.description || null,
            slug: result.slug || null,
            count: parseInt(result.count, 10),
        }));
    }

    async getAllThemesData(): Promise<IThemeData[]> {
        const query = `${this.createPrefixes(["skos", "schema", "owl", "dcat"])}
      SELECT ?iri ?title ?image (COUNT(DISTINCT ?datasetIri) as ?count)
      WHERE {
          ?iri2 a skos:Concept .
          OPTIONAL { ?iri2 schema:image ?image } .
          ?iri2 owl:sameAs ?iri .
          ?iri skos:prefLabel ?title .
          ?iri skos:topConceptOf <http://publications.europa.eu/resource/authority/data-theme> .
          OPTIONAL { ?datasetIri a dcat:Dataset ; dcat:theme ?iri . }
          FILTER ( strstarts(str(?iri), 'http://publications.europa.eu') )
          FILTER ( LANG(?title) = '${this.lang}' )
      }
      GROUP BY ?iri ?title ?image
      ORDER BY DESC(?count) ASC(?title)`;
        const results = await sparql.query<IRawThemeData>(query);
        return results.map((result) => ({
            iri: result.iri,
            title: result.title,
            image: result.image || null,
            count: parseInt(result.count, 10),
        }));
    }

    private createPrefixes(prefixes?: (keyof typeof Prefix)[]): string {
        if (!prefixes) prefixes = <(keyof typeof Prefix)[]>Object.keys(Prefix);

        return prefixes.map((name) => `PREFIX ${name}: <${Prefix[name]}>`).join("\n");
    }

    private createFilter(options: DatasetQueryOptions, datasetIriLabel = "?datasetIri"): string {
        return `
      ${
          // themes filter
          options.filter?.themeIris
              ? options.filter?.themeIris.map((themeIri) => `${datasetIriLabel} dcat:theme <${themeIri}> .`).join("\n")
              : ``
      }
      ${
          // publisher filter
          options.filter?.publisherIri ? `${datasetIriLabel} dct:publisher <${options.filter?.publisherIri}> .` : ``
      }
      ${
          // keywords filter
          options.filter?.keywords
              ? options.filter?.keywords
                    .map((keyword) => `${datasetIriLabel} dcat:keyword "${keyword}"@${this.lang} .`)
                    .join("\n")
              : ``
      }
      ${
          // format filter
          options.filter?.formatIris
              ? `${datasetIriLabel} dcat:distribution ?distributionIri .\n
            ${options.filter?.formatIris.map((formatIri) => `?distributionIri dct:format <${formatIri}> .`).join("\n")}`
              : ``
      }
    `;
    }

    private createSearch(searchString: string): string {
        const escapedSearchString = searchString.replace(/[-/\\^$*+?.()|[\]{}]/g, "\\\\$&");
        return `
      FILTER (REGEX(?title, "${escapedSearchString}", "i") || REGEX(?description, "${escapedSearchString}", "i")) .
      FILTER (LANG(?title) = "${this.lang}") .
      FILTER (LANG(?description) = "${this.lang}") .
    `;
    }

    private async mapDatasets(datasets: RawDatasetsItem[]): Promise<DatasetsItem[]> {
        const generalThemeImageQuery = `${this.createPrefixes(["schema"])}
      SELECT ?image
      WHERE { <https://lkod.rabin.golemio.cz/data-theme/general> schema:image ?image . }
    `;
        const generalThemeImage = (await sparql.query<{ image: string }>(generalThemeImageQuery))[0]?.image || undefined;

        return Promise.all(
            datasets.map(async (dataset) => ({
                iri: dataset.iri,
                title: dataset.title,
                description: dataset.description || null,
                publisher: dataset.publisher || null,
                themeNames: dataset.rawThemes?.split("|") || null,
                formats: dataset.rawFormats?.split("|") || null,
                themeImage:
                    (dataset.rawThemes?.split("|").length ?? 0) === 1
                        ? await sparql
                              .query<{ image: string }>(
                                  `${this.createPrefixes(["schema", "dcat", "owl"])}
                    SELECT ?image
                    WHERE { <${dataset.iri}> dcat:theme ?iri . ?iri2 owl:sameAs ?iri . ?iri2 schema:image ?image. }`
                              )
                              .then((result) => result[0]?.image || "")
                        : generalThemeImage || null,
            }))
        );
    }
}
