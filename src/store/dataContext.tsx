/* eslint-disable @typescript-eslint/no-empty-function */
import { useRouter } from "next/router";
import { createContext, Dispatch, ReactNode, SetStateAction, useContext, useEffect, useState } from "react";

import { genericFilter, IFilter } from "@/types/FilterInterface";
import { stringOrArr } from "@/utils/helpers";

interface IDataCtx {
    ctxFilter: IFilter & genericFilter;
    initialFilter: IFilter & genericFilter;
    ctxSearchString: string;
    setCtxFilter: Dispatch<SetStateAction<IFilter & genericFilter>>;
    setCtxSearchString: Dispatch<SetStateAction<string>>;
}

const initialFilter = {
    themeIris: [],
    publisherIri: "",
    keywords: [],
    formatIris: [],
};

const DataCtx = createContext<IDataCtx>({
    ctxFilter: initialFilter,
    initialFilter: initialFilter,
    ctxSearchString: "",
    setCtxFilter: () => {},
    setCtxSearchString: () => {},
});

export const DataCtxProvider = ({ children }: { children: ReactNode }) => {
    const [filter, setFilter] = useState<IFilter & genericFilter>(initialFilter);
    const [searchString, setSearchString] = useState("");

    const router = useRouter();

    const { publisherIri, themeIris, keywords, formatIris, search } = router.query;

    useEffect(() => {
        if (!router.isReady) {
            return;
        }
        if (router.isReady) {
            if (publisherIri) {
                setFilter((filter) => {
                    return { ...filter, publisherIri: publisherIri as string };
                });
            } else {
                setFilter((filter) => {
                    return { ...filter, publisherIri: "" };
                });
            }
            if (themeIris) {
                setFilter((filter) => {
                    return { ...filter, themeIris: stringOrArr(themeIris) };
                });
            } else {
                setFilter((filter) => {
                    return { ...filter, themeIris: [] };
                });
            }
            if (keywords) {
                setFilter((filter) => {
                    return { ...filter, keywords: stringOrArr(keywords) };
                });
            } else {
                setFilter((filter) => {
                    return { ...filter, keywords: [] };
                });
            }
            if (formatIris) {
                setFilter((filter) => {
                    return { ...filter, formatIris: stringOrArr(formatIris) };
                });
            } else {
                setFilter((filter) => {
                    return { ...filter, formatIris: [] };
                });
            }
            if (search) {
                setSearchString(search as string);
            } else {
                setSearchString("");
            }
        }
    }, [router.isReady, publisherIri, themeIris, keywords, formatIris, search]);

    return (
        <DataCtx.Provider
            value={{
                ctxFilter: filter,
                initialFilter: initialFilter,
                setCtxFilter: setFilter,
                ctxSearchString: searchString,
                setCtxSearchString: setSearchString,
            }}
        >
            {children}
        </DataCtx.Provider>
    );
};

export const useDataCtx = () => useContext(DataCtx);
