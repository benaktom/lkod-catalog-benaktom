import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();

export interface Config {
    ENDPOINT: string;
    environment: "production" | "development" | "test";
    PUBLIC_URL: string;
    ADMIN_URL: string;
    GOOGLE_SITE_VERIFICATION: string;
    SHOW_PROJECT_PAGE: "true" | "false";
    SHOW_ABOUTLKOD_PAGE: "true" | "false";
    SHOW_ADMINLOGIN_LINK: "true" | "false";
}

export const config: Config = {
    ENDPOINT: publicRuntimeConfig.ENDPOINT,
    environment: publicRuntimeConfig.environment,
    PUBLIC_URL: publicRuntimeConfig.PUBLIC_URL,
    ADMIN_URL: publicRuntimeConfig.ADMIN_URL,
    GOOGLE_SITE_VERIFICATION: publicRuntimeConfig.GOOGLE_SITE_VERIFICATION,
    SHOW_PROJECT_PAGE: publicRuntimeConfig.SHOW_PROJECT_PAGE,
    SHOW_ABOUTLKOD_PAGE: publicRuntimeConfig.SHOW_ABOUTLKOD_PAGE,
    SHOW_ADMINLOGIN_LINK: publicRuntimeConfig.SHOW_ADMINLOGIN_LINK,
};
