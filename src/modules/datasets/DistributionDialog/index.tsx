import React, { FC, useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import Button from "@/components/Button";
import { Heading } from "@/components/Heading";
import { Modal } from "@/components/Modal";
import { Distribution, DistributionLicence } from "@/schema/distribution";
import { CatalogService } from "@/services/catalog-service";
import { getCopyright, getCopyrightedDB, getPII, getSpecialDB } from "@/utils/distributionLicenceGetters";

import LicenceCard from "../LicenceCard/LicenceCard";
import { MetadataModalCard } from "../MetadataModalCard";
import styles from "./DistributionDialog.module.scss";

const service = new CatalogService();

export const DistributionDialog: FC<{ isOpen: boolean; closeDialog: () => void; distribution: Distribution }> = ({
    isOpen,
    closeDialog,
    distribution,
}) => {
    const { t } = useTranslation();
    const [distributionLicence, setDistributionLicence] = useState<DistributionLicence>();

    const copyrighted = getCopyright(distributionLicence?.["autorské-dílo"]);
    const copyrightedDB = getCopyrightedDB(distributionLicence?.["databáze-jako-autorské-dílo"]);
    const specialDB = getSpecialDB(distributionLicence?.["databáze-chráněná-zvláštními-právy"]);
    const pii = getPII(distributionLicence?.["osobní-údaje"]);

    const getLicence = useCallback(async () => {
        try {
            const distributionLicence = await service.getDistributionLicense(distribution.iri);
            setDistributionLicence(distributionLicence);
        } catch (error) {
            console.log(error);
        }
    }, [distribution.iri]);

    useEffect(() => {
        getLicence();
        window.scrollTo({ top: 10, behavior: "smooth" });
    }, [getLicence]);

    return (
        <Modal show={isOpen} onClose={closeDialog} label={t("distributionFileInfoHeading")} titleData={distribution.title ?? ""}>
            {distributionLicence && (
                <section className={styles["modal-card__data"]}>
                    <Heading tag={`h5`}>{t("heading.termsofuse")}</Heading>
                    <div className={styles["modal-card__licences"]}>
                        <div className={styles["modal-card__licences_grouped"]}>
                            <LicenceCard source={copyrighted} />
                            <LicenceCard source={copyrightedDB} />
                        </div>
                        <div className={styles["modal-card__licences_grouped"]}>
                            <LicenceCard source={specialDB} />
                            <LicenceCard source={pii} />
                        </div>
                    </div>
                </section>
            )}
            {distribution && (
                <section className={styles["modal-card__data"]}>
                    <Heading tag={`h5`}>{t("heading.metadata")}</Heading>
                    <MetadataModalCard label={t("metadataModalCard.datasetdistributionname")} data={distribution.title ?? ""} />
                    {distribution.downloadUrl && (
                        <MetadataModalCard link label={t("metadataModalCard.downloadlink")} data={distribution.downloadUrl} />
                    )}
                    {distribution.accessUrl && (
                        <MetadataModalCard link label={t("metadataModalCard.url")} data={distribution.accessUrl} />
                    )}
                    {distribution.format.title && (
                        <MetadataModalCard label={t("metadataModalCard.format")} data={distribution.format.title} />
                    )}
                    {distribution.mediaType && (
                        <MetadataModalCard label={t("metadataModalCard.mediatype")} data={distribution.mediaType} />
                    )}
                    {distribution.conformsTo && (
                        <MetadataModalCard link label={t("metadataModalCard.linktoschematic")} data={distribution.conformsTo} />
                    )}
                    {distribution.compressFormat && (
                        <MetadataModalCard
                            label={t("metadataModalCard.mediatypecompression")}
                            data={distribution.compressFormat}
                        />
                    )}
                    {distribution.packageFormat && (
                        <MetadataModalCard label={t("metadataModalCard.mediatypepackaging")} data={distribution.packageFormat} />
                    )}
                </section>
            )}
            <div className={styles["modal-card__controls"]}>
                <Button label={t("buttons.close")} color={`secondary`} onClick={closeDialog} />
            </div>
        </Modal>
    );
};
