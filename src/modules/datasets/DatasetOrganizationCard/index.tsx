import Image from "next/image";
import React, { FC } from "react";

import { Heading } from "@/components/Heading";
import Project from "@/root/project.custom.json";

import styles from "./DatasetOrganizationCard.module.scss";

type Props = { label: string; logoUrl: string; organizationUrl: string };

const errorNoImage = Project.helpers.errorNoImage;

const OrganizationCard: FC<Props> = ({ label, logoUrl }) => {
    return (
        <div className={styles["dataset-org-card"]}>
            <Heading tag={`h2`} type={`h6`} className={styles["dataset-org-card__label"]}>
                {`Organizace`}
            </Heading>
            <div
                className={`${styles["dataset-org-card__image-frame"]} ${
                    logoUrl ? "" : styles["dataset-org-card__image-frame_dull"]
                }`}
            >
                <Image src={logoUrl ? logoUrl : errorNoImage} alt={label} layout={`fill`} />
            </div>
            <Heading tag={`h3`} type={`h5`}>
                {label}
            </Heading>
        </div>
    );
};

export default OrganizationCard;
