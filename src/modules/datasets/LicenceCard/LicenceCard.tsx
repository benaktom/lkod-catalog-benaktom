import Image from "next/image";
import React from "react";

import styles from "./LicenceCard.module.scss";

type Props = {
    source: {
        text: string;
        icon: string;
        link: string | null | undefined;
    };
};

const LicenceCard = ({ source }: Props) => {
    return (
        <div className={styles["licence-card"]}>
            <div className={styles["licence-card__image"]}>
                <Image src={source.icon} alt={source.icon} />
            </div>
            <a href={source.link ?? ""}>
                <p>{source.text}</p>
            </a>
        </div>
    );
};

export default LicenceCard;
