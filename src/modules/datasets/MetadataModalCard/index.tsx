import React from "react";

import styles from "./MetadataModalCard.module.scss";

type Props = { label: string; data: string; link?: boolean };

export const MetadataModalCard = ({ label, data, link }: Props) => {
    return (
        <div className={styles["metadata-modalcard"]}>
            <p className={styles["metadata-modalcard__title"]}>{label}</p>
            {link ? (
                <a href={data} target="_blank" rel="noreferrer">
                    {data}
                </a>
            ) : (
                <p>{data}</p>
            )}
        </div>
    );
};
