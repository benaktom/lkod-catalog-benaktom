import React, { FC, useEffect } from "react";
import { useTranslation } from "react-i18next";

import Button from "@/components/Button";
import { Heading } from "@/components/Heading";
import { Modal } from "@/components/Modal";
import { DistributionService } from "@/schema/distribution";

import styles from "../DistributionDialog/DistributionDialog.module.scss";
import { MetadataModalCard } from "../MetadataModalCard";

const ServiceDialog: FC<{ isOpen: boolean; closeDialog: () => void; distributionService: DistributionService }> = ({
    isOpen,
    closeDialog,
    distributionService,
}) => {
    const { t } = useTranslation();

    useEffect(() => {
        window.scrollTo({ top: 10, behavior: "smooth" });
    }, []);

    return (
        <Modal show={isOpen} onClose={closeDialog} label={t("distributionServiceInfoHeading")}>
            {distributionService && (
                <section className={styles["modal-card__data"]}>
                    <Heading tag={`h5`}>{t("heading.metadata")}</Heading>
                    <MetadataModalCard
                        label={t("metadataModalCard.datasetdistributionname")}
                        data={distributionService.title ?? ""}
                    />
                    {distributionService.endpointURL && (
                        <MetadataModalCard
                            link
                            label={t("metadataModalCard.accesspoint")}
                            data={distributionService.endpointURL}
                        />
                    )}
                    {distributionService.endpointDescription && (
                        <MetadataModalCard
                            link
                            label={t("metadataModalCard.accesspointDescription")}
                            data={distributionService.endpointDescription}
                        />
                    )}
                    {distributionService.conformsTo && (
                        <MetadataModalCard
                            link
                            label={t("metadataModalCard.linktospecification")}
                            data={distributionService.conformsTo}
                        />
                    )}
                </section>
            )}
            <div className={styles["modal-card__controls"]}>
                <Button label={t("buttons.close")} color={`secondary`} onClick={closeDialog} />
            </div>
        </Modal>
    );
};

export default ServiceDialog;
