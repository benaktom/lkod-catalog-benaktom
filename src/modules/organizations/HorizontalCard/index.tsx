import Image from "next/image";
import React, { DetailedHTMLProps, forwardRef, HTMLAttributes } from "react";

import { Card } from "@/components/Card";
import Project from "@/root/project.custom.json";

import styles from "./HorizontalCard.module.scss";

const errorNoImage = Project.helpers.errorNoImage;
interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    label: string;
    logoUrl?: string;
    count: number;
    onClick: () => void;
}

const HorizontalCard = forwardRef<HTMLDivElement, Props>(({ label, logoUrl, count, onClick }, ref) => {
    return (
        <Card tag={`button`} horizontal="true" ref={ref} onClick={onClick} label={label}>
            <span className={`${styles["horizontal-card"]}`}>
                <span
                    className={`${styles["horizontal-card__image-frame"]} ${
                        logoUrl ? "" : styles["horizontal-card__image-frame_dull"]
                    }`}
                >
                    <Image
                        src={logoUrl ? logoUrl : errorNoImage}
                        alt={label}
                        layout="fill"
                        sizes="(max-width: 576px) 100vw, (max-width: 992px) 33vw, 25vw"
                    />
                </span>
                <span>
                    <span className={styles["horizontal-card__label"]}>{label}</span>
                </span>
                <span>
                    <span className={styles["horizontal-card__link"]}>{`${count} ${
                        count > 4 || count === 0 ? "datových sad" : count > 1 ? "datové sady" : "datová sada"
                    }`}</span>
                </span>
            </span>
        </Card>
    );
});

HorizontalCard.displayName = "HorizontalCard";

export default HorizontalCard;
