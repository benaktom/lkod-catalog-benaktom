import Image from "next/image";
import React, { FC } from "react";

import { Card } from "@/components/Card";
import ColorLink from "@/components/ColorLink";
import Project from "@/root/project.custom.json";

import styles from "./OrganizationCard.module.scss";

const errorNoImage = Project.helpers.errorNoImage;

type Props = { label: string; logoUrl: string; description: string | null; count: number; onClick: () => void };

export const OrganizationCard: FC<Props> = ({ label, logoUrl, count, onClick }) => {
    return (
        <Card tag={`button`} onClick={onClick} label={label}>
            <span className={styles["organization-card"]}>
                <span
                    className={`${styles["organization-card__image-frame"]} ${
                        logoUrl ? "" : styles["organization-card__image-frame_dull"]
                    }`}
                >
                    <Image
                        src={logoUrl ? logoUrl : errorNoImage}
                        alt={label}
                        layout="fill"
                        sizes="(max-width: 576px) 100vw, (max-width: 992px) 33vw, 25vw"
                    />
                </span>
                <span className={styles["organization-card__label"]}>{label}</span>
                <span className={styles["organization-card__link"]}>
                    <ColorLink
                        linkText={`${count} ${
                            count > 4 || count === 0 ? "datových sad" : count > 1 ? "datové sady" : "datová sada"
                        }`}
                        direction={`right`}
                        start
                    />
                </span>
            </span>
        </Card>
    );
};
