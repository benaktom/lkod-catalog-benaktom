import { useRouter } from "next/router";
import React, { FC } from "react";
import { useTranslation } from "react-i18next";

import Button from "@/components/Button";
import { ButtonReset } from "@/components/ButtonReset";
import { Spinner } from "@/components/Spinner";
import { useFilters } from "@/hooks/useFilters";
import useWindowSize, { ISize } from "@/hooks/useWindowSize";
import { useDataCtx } from "@/root/store/dataContext";

import { FilterFormSection } from "../FilterFormSection";
import styles from "./FilterForm.module.scss";

type Props = {
    onClose: () => void;
    publisherIri?: string;
};

export const FilterForm: FC<Props> = ({ onClose, publisherIri }) => {
    const { t } = useTranslation();
    const router = useRouter();

    const size: ISize = useWindowSize();
    const organization = router.query.organization;
    const { publishers, topics, keywords, formats, isLoading } = useFilters();

    const { ctxFilter } = useDataCtx();
    if (publisherIri) {
        ctxFilter.publisherIri = publisherIri;
    }

    const filterHandler = (val: string, key: string) => {
        if (router.query[key] === val) {
            router.push(
                {
                    query: { ...router.query, [key]: undefined },
                },
                undefined,
                { shallow: true }
            );
        } else if (router.query[key] && typeof router.query[key] !== "string" && router.query[key]?.includes(val)) {
            const filteredArr = (router.query[key] as string[]).filter((el) => el !== val);
            router.push(
                {
                    query: { ...router.query, [key]: filteredArr },
                },
                undefined,
                { shallow: true }
            );
        } else {
            if (router.query[key]) {
                let newArr = [] as string[];
                if (typeof router.query[key] === "string") {
                    newArr = [router.query[key] as string, val];
                } else {
                    const filterArr = router.query[key] as string[];
                    newArr = [...filterArr, val];
                }
                router.push({ query: { ...router.query, [key]: newArr } }, undefined, { shallow: true });
            } else {
                router.push({ query: { ...router.query, [key]: val } }, undefined, { shallow: true });
            }
        }
        if (size.width && size.width <= 991) {
            document.getElementById(key)?.scrollIntoView({ behavior: "smooth", block: "center" });
        }
    };

    return (
        <>
            <div className={styles.wrapper}>
                {isLoading && <Spinner />}
                {publishers && publishers?.length > 0 && !publisherIri && (
                    <FilterFormSection
                        className={styles["filter-section"]}
                        label={t("filtersSection.organization")}
                        section={`publisherIri`}
                        data={publishers}
                        fnc={filterHandler}
                        checkerCondition={ctxFilter.publisherIri as string}
                        limit={4}
                    />
                )}
                {topics && topics.length > 0 && (
                    <FilterFormSection
                        className={styles["filter-section"]}
                        label={t("filtersSection.theme")}
                        section={`themeIris`}
                        data={topics}
                        fnc={filterHandler}
                        checkerCondition={ctxFilter.themeIris as string[]}
                        limit={4}
                    />
                )}
                {keywords && (
                    <FilterFormSection
                        className={styles["filter-section"]}
                        label={t("filtersSection.keywords")}
                        data={keywords}
                        section={`keywords`}
                        fnc={filterHandler}
                        checkerCondition={ctxFilter.keywords as string[]}
                        limit={4}
                    />
                )}
                {formats && (
                    <FilterFormSection
                        className={styles["filter-section"]}
                        data={formats}
                        label={t("filtersSection.formats")}
                        section={`formatIris`}
                        fnc={filterHandler}
                        checkerCondition={ctxFilter.formatIris as string[]}
                        limit={4}
                    />
                )}
            </div>
            <section className={styles["filter-section__form-actions"]}>
                {size.width && size.width <= 991 && (
                    <Button
                        label={t("filters.confirmFilters")}
                        color={`secondary`}
                        onClick={() => {
                            onClose();
                        }}
                    />
                )}

                <ButtonReset
                    label={t("filters.cancelFilters")}
                    onClick={() => {
                        if (organization) {
                            router.push({ pathname: `/organizations/${organization}` }, undefined, {
                                shallow: false,
                            });
                        } else {
                            router.push({ query: undefined });
                        }
                    }}
                />
            </section>
        </>
    );
};
