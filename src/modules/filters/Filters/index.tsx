import React, { FC, useState } from "react";
import { useTranslation } from "react-i18next";

import Button from "@/components/Button";
import { Heading } from "@/components/Heading";
import { FilterIcon } from "@/components/icons/FilterIcon";
import { Modal } from "@/components/Modal";
import useWindowSize from "@/hooks/useWindowSize";
import { ISize } from "@/hooks/useWindowSize";

import { FilterForm } from "../FilterForm";
import styles from "./Filters.module.scss";

type Props = {
    label: string;
    publisherIri?: string;
};

const Filters: FC<Props> = ({ label, publisherIri }) => {
    const [showModal, setShowModal] = useState(false);
    const size: ISize = useWindowSize();
    const { t } = useTranslation();

    const modalHandler = () => {
        setShowModal(true);
        window.scrollTo({ top: 0, behavior: "smooth" });
    };

    const closeHandler = () => {
        setShowModal(false);
    };

    return (
        <div className={styles.filters} tabIndex={0}>
            <Heading tag={`h3`} type={`h4`}>
                {label}
            </Heading>
            <div className={styles["filter-list"]}>
                {size.width && size.width < 991 && (
                    <Button color="outline-secondary" label={t("filters.filtersdatasets")} onClick={() => modalHandler()}>
                        <FilterIcon color={`secondary`} width={22} height={22} />
                    </Button>
                )}
                <Modal show={showModal} onClose={() => setShowModal(false)} label={t("filters.title")}>
                    <FilterForm onClose={closeHandler} publisherIri={publisherIri} />
                </Modal>
                {size.width && size.width > 991 && <FilterForm onClose={closeHandler} publisherIri={publisherIri} />}
            </div>
        </div>
    );
};

export default Filters;
