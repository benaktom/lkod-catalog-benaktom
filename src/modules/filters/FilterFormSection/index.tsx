/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { usePlausible } from "next-plausible";
import React, { MouseEvent, useState } from "react";

import { CheckBox } from "@/components/CheckBox";
import { Heading } from "@/components/Heading";
import { ListLimiter } from "@/components/ListLimiter";

type Props = {
    className: string;
    checkerCondition: string | string[];
    data: {
        iri?: string;
        label: string;
        count: string;
    }[];
    fnc: (val: string, key: string) => void;
    label: string;
    limit: number;
    section: string;
};

export const FilterFormSection = ({ className, checkerCondition, data, fnc, label, limit, section }: Props) => {
    const [listLimit, setListLimit] = useState<number>(limit);
    const plausible = usePlausible();

    const limitHandler = (e: MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        if (listLimit + limit < data.length) {
            setListLimit((prev) => prev + limit);
        } else if (listLimit + limit >= data.length) setListLimit(data.length);
        if (listLimit >= data.length) {
            setListLimit(limit);
        }
    };

    const checkboxHandler = (filter: { iri?: string; label: string; count: string }) => {
        !filter.iri ? fnc(filter.label, section) : fnc(filter.iri, section);
        plausible("Filter+Click", {
            props: {
                filter: filter.label,
            },
        });
    };

    return (
        <div className={className} tabIndex={0}>
            <fieldset id={section}>
                <legend>
                    <Heading tag={`h4`} type={`h5`}>
                        {label}
                    </Heading>
                    <span aria-label={`${data.length} filtrů`} role="marquee">
                        {data.length}
                    </span>
                </legend>
                {data &&
                    data.slice(0, listLimit).map((el) => {
                        return (
                            <CheckBox
                                key={!el.iri ? el.label : el.iri}
                                label={el.label}
                                count={el.count}
                                id={el.iri?.replace(/\s/g, "") ?? el.label.replace(/\s/g, "")}
                                checked={
                                    checkerCondition
                                        ? typeof checkerCondition === "string"
                                            ? checkerCondition === el.iri
                                            : checkerCondition.includes(!el.iri ? el.label : el.iri!)
                                        : false
                                }
                                onClick={() => checkboxHandler(el)}
                            />
                        );
                    })}
                {data.length > limit && <ListLimiter rest={data.length - listLimit} limit={limit} onClick={limitHandler} />}
            </fieldset>
        </div>
    );
};
