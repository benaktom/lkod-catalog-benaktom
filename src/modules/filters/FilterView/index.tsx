import { useRouter } from "next/router";
import { useTranslation } from "react-i18next";

import { ButtonReset } from "@/components/ButtonReset";
import { Chip } from "@/components/Chip";
import { useFilterChips } from "@/hooks/useFilterChips";

import styles from "./FilterView.module.scss";

type Props = {
    orgPage?: boolean;
};
const FilterView = ({ orgPage }: Props) => {
    const { t } = useTranslation();
    const router = useRouter();

    const organization = router.query.organization;
    const { publishers, topics, keys, formats } = useFilterChips();
    const show =
        (formats && formats.length > 0) ||
        (keys && keys.length > 0) ||
        (publishers && publishers.length > 0) ||
        (topics && topics.length > 0);

    const filterQuery = (key: string, val: string) => {
        return router.push(
            {
                pathname: "/datasets",
                query: {
                    ...router.query,
                    [key]:
                        typeof router.query[key] === "string" ? "" : (router.query[key] as string[]).filter((el) => el !== val),
                },
            },
            undefined,
            { scroll: false }
        );
    };

    return (
        <div className={styles["filter-view"]}>
            {show && (
                <>
                    Zvolené filtry:
                    <ul>
                        {!orgPage && (
                            <li>
                                {publishers && publishers.length > 0 && (
                                    <Chip
                                        label={publishers[0].label}
                                        onClick={() => {
                                            filterQuery("publisherIri", publishers[0].iri);
                                        }}
                                    />
                                )}
                            </li>
                        )}
                        {topics &&
                            topics.length > 0 &&
                            topics.map((theme) => {
                                return (
                                    <li key={theme.iri}>
                                        <Chip
                                            label={theme.label}
                                            onClick={() => {
                                                filterQuery("themeIris", theme.iri);
                                            }}
                                        />
                                    </li>
                                );
                            })}
                        {keys &&
                            keys.length > 0 &&
                            keys.map((keyword) => {
                                return (
                                    <li key={keyword.label}>
                                        <Chip
                                            label={keyword.label}
                                            onClick={() => {
                                                filterQuery("keywords", keyword.label);
                                            }}
                                        />
                                    </li>
                                );
                            })}
                        {formats &&
                            formats.length > 0 &&
                            formats.map((format) => {
                                return (
                                    <li key={format.iri}>
                                        <Chip
                                            label={format.label}
                                            onClick={() => {
                                                filterQuery("formatIris", format.iri);
                                            }}
                                        />
                                    </li>
                                );
                            })}
                    </ul>
                    <ButtonReset
                        color={`secondary`}
                        label={t("filters.cancelFilters")}
                        onClick={() => {
                            if (organization) {
                                router.push({ pathname: `/organizations/${organization}` }, undefined, {
                                    shallow: false,
                                });
                            } else {
                                router.push({ query: undefined });
                            }
                        }}
                        end
                    />
                </>
            )}
        </div>
    );
};

export default FilterView;
