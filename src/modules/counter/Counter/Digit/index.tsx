import React, { DetailedHTMLProps, FC, HTMLAttributes } from "react";

import styles from "./Digit.module.scss";

interface DigitProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    amount: string | number;
    caption: string;
}

export const Digit: FC<DigitProps> = ({ amount, caption, ...restProps }) => {
    return (
        <div className={styles["digit"]} {...restProps}>
            <span className={styles["digit_amount"]}>{amount.toLocaleString()}</span>
            <span className={styles["digit_caption"]}>{caption}</span>
        </div>
    );
};
