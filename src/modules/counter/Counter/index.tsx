import Link from "next/link";
import React, { FC } from "react";

import styles from "./Counter.module.scss";
import { Digit } from "./Digit";

type CounterProps = {
    datasetsCount: number;
    themesCount: string;
    orgsCount: number;
    onClick: () => void;
};

const Underline = () => {
    return <span className={styles.underline}></span>;
};

export const Counter: FC<CounterProps> = ({ datasetsCount, themesCount, orgsCount, onClick }) => {
    return (
        <div className={styles["counter-container"]}>
            <div className={styles.counter}>
                <Link href={`/organizations`}>
                    <a>
                        <Digit amount={orgsCount} caption="organizací" />
                    </a>
                </Link>
                <Underline />
                <Link href={`/datasets`}>
                    <a>
                        <Digit amount={datasetsCount} caption="datových sad" />
                    </a>
                </Link>
                <Underline />
                <Link href="#topicList">
                    <a onClick={onClick}>
                        <Digit amount={themesCount} caption="témat" />
                    </a>
                </Link>
            </div>
        </div>
    );
};
