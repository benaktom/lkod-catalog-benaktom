import { useRouter } from "next/router";
import { usePlausible } from "next-plausible";
import React, { forwardRef, KeyboardEvent, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import Button from "@/components/Button";
import { Heading } from "@/components/Heading";
import { SearchInput } from "@/components/SearchInput";

import styles from "./SearchForm.module.scss";

type SearchFormProps = {
    label?: string;
    fullWidth?: boolean;
    placeholder?: string;
};

// eslint-disable-next-line react/display-name
export const SearchForm = forwardRef<HTMLDivElement, SearchFormProps>((SearchFormProps, ref) => {
    const { t } = useTranslation();
    const { label, fullWidth, placeholder = `${t("searchString.placeholderText")}` } = SearchFormProps;
    const router = useRouter();
    const { search } = router.query;
    const plausible = usePlausible();

    const [inputValue, setInputValue] = useState("");

    const updateInputValue = (value: string) => {
        setInputValue(value);
    };

    const searchClicked = () => {
        router.push({ pathname: "/datasets", query: { search: inputValue } }, undefined, { scroll: false });
        plausible("SearchEvent: Search", {
            props: {
                inputValue,
            },
        });
    };

    const onKeyHandler = (event: KeyboardEvent) => {
        if (event.key === "Enter") {
            searchClicked();
        }
    };

    const resetInputField = () => {
        updateInputValue("");
        router.push({
            pathname: router.pathname,
            query: { ...router.query, search: "" },
        });
    };

    useEffect(() => {
        if (!router.isReady) {
            return;
        }
        if (!search) {
            setInputValue("");
        } else {
            setInputValue(search as string);
        }
    }, [router.isReady, search]);

    return (
        <div className={`${styles["search-form"]} ${fullWidth ? styles.full : ""}`} ref={ref}>
            <div className={styles.row}>
                {label ? (
                    <Heading tag={`h2`} type={`h4`}>
                        {label}
                    </Heading>
                ) : (
                    ""
                )}
            </div>
            <div className={styles.row}>
                <SearchInput
                    name="dataSearch"
                    aria-labelledby="searchButton"
                    placeholder={placeholder}
                    onKeyDown={onKeyHandler}
                    resetInputField={resetInputField}
                    updateInputValue={updateInputValue}
                    value={inputValue}
                />
                <Button id="searchButton" color={`secondary`} label="Vyhledat" onClick={searchClicked} />
            </div>
        </div>
    );
});
