import { useRouter } from "next/router";
import { usePlausible } from "next-plausible";
import React, { DetailedHTMLProps, forwardRef, HTMLAttributes, useState } from "react";
import { useTranslation } from "react-i18next";

import ColorLink from "@/components/ColorLink";
import { Heading } from "@/components/Heading";
import TopicCard from "@/modules/topics/TopicCard";
import { useDataCtx } from "@/root/store/dataContext";
import { IThemeData } from "@/schema/common";

import styles from "./TopicList.module.scss";

const PER_PAGE = 8;

interface TopicList extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    label: string;
    data: IThemeData[];
}

const TopicList = forwardRef<HTMLDivElement, TopicList>(({ label, data }, ref) => {
    const { t } = useTranslation();
    const router = useRouter();
    const plausible = usePlausible();

    const { initialFilter, setCtxFilter } = useDataCtx();

    const [perPage, setPerPage] = useState(PER_PAGE);

    const showHandler = () => {
        if (perPage === PER_PAGE) {
            setPerPage(data.length);
        } else setPerPage(PER_PAGE);
    };

    const pushTopicHandler = (topic: IThemeData) => {
        topic.count > 0
            ? (router.push(
                  {
                      pathname: "/datasets",
                      query: { themeIris: topic.iri },
                  },
                  undefined,
                  { shallow: true }
              ),
              setCtxFilter({ ...initialFilter, themeIris: [topic.iri] }))
            : router.push({ pathname: router.pathname }, undefined, { shallow: true });
        plausible("Topic+Click", {
            props: {
                topic: topic.title,
            },
        });
    };

    return (
        <div className={styles["topic-list"]} ref={ref}>
            <Heading tag={`h2`} className={styles.inner}>
                {label}
            </Heading>
            <div className={styles.container}>
                {data.slice(0, perPage).map((topic) => {
                    return (
                        <TopicCard
                            key={topic.title}
                            label={topic.title}
                            topic={topic.image ? topic.image : ""}
                            count={topic.count}
                            onClick={() => pushTopicHandler(topic)}
                        />
                    );
                })}
            </div>
            {perPage === PER_PAGE && (
                <ColorLink
                    className={styles["color-link"]}
                    linkText={t("colorLink.linkTextall")}
                    onClick={showHandler}
                    center
                    direction={`down`}
                />
            )}
            {perPage !== PER_PAGE && (
                <ColorLink
                    className={styles["color-link"]}
                    style={{ marginTop: "2rem" }}
                    linkText={t("colorLink.linkText")}
                    onClick={showHandler}
                    center
                    direction={`up`}
                />
            )}
        </div>
    );
});

export default TopicList;
