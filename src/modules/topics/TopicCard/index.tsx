import React, { FC } from "react";

import { Card } from "@/components/Card";
import IconExtSource from "@/components/IconExtSource";

import styles from "./TopicCard.module.scss";

type TopicProps = { label: string; count: number; topic: string; onClick: () => void };

const TopicCard: FC<TopicProps> = ({ label, count, topic, onClick }) => {
    return (
        <Card tag={`button`} className={styles["topic-card"]} onClick={onClick} label={label}>
            <span>
                <IconExtSource src={topic as string} alt={label} />
            </span>
            <span className={styles["topic-card__label"]}>{label}</span>
            <span className={styles["topic-card__link"]}>{`${count}
                ${count > 4 || count === 0 ? "datových sad" : count > 1 ? "datové sady" : "datová sada"}`}</span>
        </Card>
    );
};

export default TopicCard;
