# LKOD Catalog (benaktom)

This repository is fork of [Golemio LKOD Catalog](https://gitlab.com/operator-ict/golemio/lkod/lkod-catalog) and it was created as an example of LKOD Catalog customization described in [Golemio LKOD demo](https://gitlab.com/operator-ict/golemio/lkod/lkod-general/-/tree/master/demo#9-optional-basic-site-customization).

**See the result: [lkod-catalog.dev.tomas.benak-web.cz](https://lkod-catalog.dev.tomas.benak-web.cz/)**
