/** @type {import('next').NextConfig} */

const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,
    images: {
        domains: [
            "rabingolemio.blob.core.windows.net",
            "golemgolemio.blob.core.windows.net",
            "img.icons8.com",
            "publications.europa.eu",
        ],
        minimumCacheTTL: 120,
        deviceSizes: [828, 1200],
        formats: ["image/webp"],
    },
    i18n: {
        locales: ["cs-CZ"],
        defaultLocale: "cs-CZ",
    },
    async redirects() {
        return [
            {
                source: "/dataset/:path*",
                destination: "/datasets",
                permanent: true,
            },
            {
                source: "/organization/:path*",
                destination: "/organizations",
                permanent: true,
            },
            {
                source: "/group/:path*",
                destination: "/",
                permanent: true,
            },
            {
                source: "/about",
                destination: "/project",
                permanent: true,
            },
        ];
    },
    publicRuntimeConfig: {
        ENDPOINT: process.env.ENDPOINT,
        environment: process.env.NODE_ENV,
        PUBLIC_URL: process.env.PUBLIC_URL,
        ADMIN_URL: process.env.ADMIN_URL,
        GOOGLE_SITE_VERIFICATION: process.env.GOOGLE_SITE_VERIFICATION,
        SHOW_PROJECT_PAGE: process.env.SHOW_PROJECT_PAGE,
        SHOW_ABOUTLKOD_PAGE: process.env.SHOW_ABOUTLKOD_PAGE,
        SHOW_ADMINLOGIN_LINK: process.env.SHOW_ADMINLOGIN_LINK,
    },
};

// for usual case
module.exports = nextConfig;
